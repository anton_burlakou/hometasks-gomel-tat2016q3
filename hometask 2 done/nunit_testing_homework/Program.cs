﻿namespace Nunit_testing_homework
{
    using Nunit_testing_framework;
    using Nunit_testing_framework.Test;
    using System;
    using System.Collections;
    using System.Linq;
    public class Program
    {
        public static void Main(string[] args)
        {
            #region BoyTest
            BoyTest boyTest = new BoyTest();
            boyTest.Setup();

            var boyParam = BoyTest.SummerMonthProp.First();
            boyTest.SummerMonth(boyParam);
            Console.WriteLine("Positive test summer month passed with params {0}", boyParam);

            boyParam = BoyTest.NoSummerMonthProp.First();
            boyTest.NotSummerMonth(boyParam);
            Console.WriteLine("Negative test summer month passed with params {0}", boyParam);

            boyParam = BoyTest.RichBoyProp.First();
            boyTest.Affluent(boyParam);
            Console.WriteLine("Positive test affluent passed with params {0}", boyParam);

            boyParam = BoyTest.PoorBoyProp.First();
            boyTest.Indigent(boyParam);
            Console.WriteLine("Negative test indigent passed with params {0}", boyParam);

            boyParam = BoyTest.PrettyGirlFriendProp.First();
            boyTest.PrettyGirl(boyParam);
            Console.WriteLine("Positive test pretty girl passed with params {0}", boyParam);

            boyParam = BoyTest.AvailabilityOrNotPrettyGirlProp.First();
            boyTest.AvailabilityOrNotPrettyGirl(boyParam);
            Console.WriteLine("Negative test AvailabilityOrNotPrettyGirl girl passed with params {0}", boyParam);

            boyTest.SpendSomeMoney();
            Console.WriteLine("Positive test Spend Money passed");

            boyTest.NotEnoughMoney();
            Console.WriteLine("Negative test Not enough money passed");

            boyTest.GetMood();
            Console.WriteLine("Positive test Mood passed");
            #endregion

            #region GirlTest
            GirlTest girlTest = new GirlTest();
            girlTest.SetUp();

            var girlParam = GirlTest.RichBoyProp.First();
            girlTest.Affluent(girlParam);
            Console.WriteLine("Positive test affluent passed with params {0}", girlParam);

            girlParam = GirlTest.PoorBoyProp.First();
            girlTest.Indigent(girlParam);
            Console.WriteLine("Negative test indigent passed with params {0}", girlParam);

            girlParam = GirlTest.RichBoy2Prop.First();
            girlTest.PurchaseShoes(girlParam);
            Console.WriteLine("Positive test purchase shoes passed with params {0}", girlParam);

            girlParam = GirlTest.PoorBoyProp.First();
            girlTest.NotPurchaseShoes(girlParam);
            Console.WriteLine("Negative test not purchase shoes passed with params {0}", girlParam);

            girlParam = GirlTest.SlimFriendBoyProp.First();
            girlTest.Slim(girlParam);
            Console.WriteLine("Positive test Slim passed with params {0}", girlParam);

            girlParam = GirlTest.FatFriendBoyProp.First();
            girlTest.Fat(girlParam);
            Console.WriteLine("Negative test Fat passed with params {0}", girlParam);

            girlParam = GirlTest.ConfirmPurchaseProp.First();
            girlTest.ConfirmPurchase(girlParam);
            Console.WriteLine("Positive test confirm purchase passed with params {0}", girlParam);

            girlParam = GirlTest.UnconfirmPurchaseProp.First();
            girlTest.UnconfirmPurchase(girlParam);
            Console.WriteLine("Negative test unconfirm purchase passed with params {0}", girlParam);

            girlTest.GetMood();
            Console.WriteLine("Positive test Mood passed");
            #endregion

            Console.ReadKey();          
        }
    }
}
