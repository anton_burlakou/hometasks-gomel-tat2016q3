﻿namespace Nunit_testing_framework
{
    public class Girl : Human
    {
        public Girl(bool isPretty, bool isSlimFriendGotAFewKilos, Boy boyFriend)
        {
            this.IsPretty = isPretty;
            this.IsSlimFriendGotAFewKilos = isSlimFriendGotAFewKilos;
            this.BoyFriend = boyFriend;
            if (BoyFriend != null)
            {
                this.BoyFriend.GirlFriend = this;
            }
        }

        public Girl(bool isPretty, bool isSlimFriendGotAFewKilos) : this(isPretty, isSlimFriendGotAFewKilos, null)
        {            
        }

        public Girl(bool isPretty) : this(isPretty, false, null)
        {
        }

        public Girl() : this(false, true, null)
        {
        }       

        public bool IsPretty { get; }

        public Boy BoyFriend { get; set; }

        private bool IsSlimFriendGotAFewKilos { get; }

        public override Mood GetMood()
        {
            if (IsBoyFriendWillBuyNewShoes())
            {
                return Mood.Excellent;
            }
            else if (IsPretty || IsBoyFriendRich())
            {
                return Mood.Good;
            }
            else if (IsSlimFriendBecameFat())
            {
                return Mood.Neutral;
            }
            
            return Mood.IHateThemAll;
        }

        public void SpendBoyFriendMoney(double amountForSpending)
        {
            if (IsBoyFriendRich())
            {
                BoyFriend.SpendSomeMoney(amountForSpending);
            }
        }

        public bool IsBoyFriendRich()
        {
            return BoyFriend != null && BoyFriend.IsRich();  
        }

        public bool IsBoyFriendWillBuyNewShoes()
        {
            return IsBoyFriendRich() && IsPretty;
        }

        public bool IsSlimFriendBecameFat()
        {
            return BoyFriend != null && !IsSlimFriendGotAFewKilos; 
        }

        public override string ToString()
        {
            return string.Format("Girl: isPretty - {0}, isSlimFriendGotAFewKilos - {1}, boyfriend - {2}", IsPretty, IsSlimFriendGotAFewKilos, BoyFriend?.GetBoyInfo());
        }

        public string GetGirlInfo()
        {
            return string.Format("Girl: isPretty - {0}, isSlimFriendGotAFewKilos - {1}", IsPretty, IsSlimFriendBecameFat());
        }
    }
}