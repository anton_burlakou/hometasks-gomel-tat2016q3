﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Nunit_testing_framework
{
    public class Boy : Human
    {
        private const int Richness = 1000000;

        public Boy(Month birthdayMonth, double wealth, Girl girlFriend)
        {
            this.BirthdayMonth = birthdayMonth;
            this.Wealth = wealth;
            this.GirlFriend = girlFriend;
            if (GirlFriend != null)
            {
                this.GirlFriend.BoyFriend = this;
            }           
        }

        public Boy(Month birthdayMonth, double wealth) : this(birthdayMonth, wealth, null)
        {
        }

        public Boy(Month birthdayMonth) : this(birthdayMonth, 0, null)
        {
            this.BirthdayMonth = birthdayMonth;
            this.Wealth = 0;
            this.GirlFriend = null;
        }

        public Girl GirlFriend { get; set; }

        public double Wealth { get; set; }

        private Month BirthdayMonth { get; set; }

        public override Mood GetMood()
        {
            if (IsRich() && IsPrettyGirlFriend() && IsSummerMonth())
            {
                return Mood.Excellent;
            }
            else if (IsRich() && IsPrettyGirlFriend())
            {
                return Mood.Good;
            }
            else if (IsRich() && IsSummerMonth())
            {
                return Mood.Neutral;
            }
            else if (IsRich() || IsPrettyGirlFriend() || IsSummerMonth())
            {
                return Mood.Bad;
            }

            return Mood.Horrible;
        }

        public void SpendSomeMoney(double amountForSpending)
        {
            if (Wealth >= amountForSpending)
            {
                Wealth -= amountForSpending;
            }
            else
            {
                throw new NullReferenceException(string.Format("Not enough money! Requested amount is {0}$ but you can't spend more then {1}$", amountForSpending, Wealth));
            }
        }

        public bool IsSummerMonth()
        {
            var summerMonthes = new List<Month>() { Month.June, Month.July, Month.August };            
            return summerMonthes.Any(m => m == BirthdayMonth);         
        }

        public bool IsRich()
        {
            return Wealth >= Richness;
        }

        public bool IsPrettyGirlFriend()
        {
            return GirlFriend != null && GirlFriend.IsPretty;
        }

        public override string ToString()
        {
            return string.Format("Boy: \nbirthday - {0}, \nwealth - {1}, \ngirlfriend - {2}", BirthdayMonth, Wealth, GirlFriend?.GetGirlInfo());
        }

        public string GetBoyInfo()
        {
            return string.Format("Boy: birthday - {0}, wealth - {1}", BirthdayMonth, Wealth);
        }
    }
}