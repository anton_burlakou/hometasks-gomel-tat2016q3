﻿using System;
using NUnit.Framework;

namespace Nunit_testing_framework.Test
{
    [TestFixture]
    public class BoyTest
    {
        private const double Richness = 100000000;
        private const double Poor = 10000;
        #region month
        private static Boy[] summerMonth =
        {
            new Boy(Month.July),
            new Boy(Month.June),
            new Boy(Month.August)
        };

        private static Boy[] notSummerMonth =
        {
            new Boy(Month.October),
            new Boy(Month.April)
        };
        #endregion

        #region wealth
        private static Boy[] richBoy =
        {
            new Boy(Month.October, Richness),
        };

        private static Boy[] poorBoy =
        {
            new Boy(Month.October, Poor),
            new Boy(Month.April)
        };
        #endregion

        #region girlfriend
        private static Boy[] prettyGirlFriend =
        {
            new Boy(Month.October, Richness, new Girl(true))
        };

        private static Boy[] availabilityOrNotPrettyGirl =
        {
            new Boy(Month.October),
            new Boy(Month.October, Richness),
            new Boy(Month.October, Richness, new Girl(false))
        };
        #endregion

        private Girl prettyGirl = new Girl(true);

        private Boy boyWithExcelentMood;
        private Boy boyWithGoodMood;
        private Boy boyWithNeutralMood;
        private Boy boyWithBadMood;
        private Boy boyWithHorribleMood;

        public static Boy[] SummerMonthProp { get; set; }

        public static Boy[] NoSummerMonthProp { get; set; }

        public static Boy[] RichBoyProp { get; set; }

        public static Boy[] PoorBoyProp { get; set; }

        public static Boy[] PrettyGirlFriendProp { get; set; }

        public static Boy[] AvailabilityOrNotPrettyGirlProp { get; set; }

        [SetUp]
        public void Setup()
        {
            SummerMonthProp = summerMonth;
            NoSummerMonthProp = notSummerMonth;
            RichBoyProp = richBoy;
            PoorBoyProp = poorBoy;
            PrettyGirlFriendProp = prettyGirlFriend;
            AvailabilityOrNotPrettyGirlProp = availabilityOrNotPrettyGirl;
            boyWithExcelentMood = new Boy(Month.August, Richness, prettyGirl);
            boyWithGoodMood = new Boy(Month.February, Richness, prettyGirl);
            boyWithNeutralMood = new Boy(Month.July, Richness);
            boyWithBadMood = new Boy(Month.July, Poor);
            boyWithHorribleMood = new Boy(Month.January);
        }

        #region test month
        [Test, TestCaseSource("summerMonth")]
        public void SummerMonth(Boy boy)
        {
            Assert.True(boy.IsSummerMonth());
        }

        [Test, TestCaseSource("notSummerMonth")]
        public void NotSummerMonth(Boy boy)
        {
            Assert.False(boy.IsSummerMonth());
        }
        #endregion

        #region test wealth
        [Test, TestCaseSource("richBoy")]
        public void Affluent(Boy boy)
        {
            Assert.True(boy.IsRich());
        }

        [Test, TestCaseSource("poorBoy")]
        public void Indigent(Boy boy)
        {
            Assert.False(boy.IsRich());
        }
        #endregion

        #region test girlfriend
        [Test, TestCaseSource("prettyGirlFriend")]
        public void PrettyGirl(Boy boy)
        {
            Assert.True(boy.IsPrettyGirlFriend());
        }

        [Test, TestCaseSource("availabilityOrNotPrettyGirl")]
        public void AvailabilityOrNotPrettyGirl(Boy boy)
        {
            Assert.False(boy.IsPrettyGirlFriend());
        }
        #endregion

        #region test spendMoney
        [Test]
        public void SpendSomeMoney()
        {
            boyWithBadMood.SpendSomeMoney(1000);
            Assert.AreEqual(boyWithBadMood.Wealth, 9000);
            boyWithBadMood.SpendSomeMoney(9000);
            Assert.AreEqual(boyWithBadMood.Wealth, 0);
        }

        [Test]
        ////[ExpectedException(typeof(NullReferenceException))]
        public void NotEnoughMoney()
        {
            Assert.Throws(typeof(NullReferenceException), () => boyWithBadMood.SpendSomeMoney(Richness));
        }
        #endregion

        #region test Mood
        [Test]
        public void GetMood()
        {
            Assert.AreEqual(boyWithExcelentMood.GetMood(), Mood.Excellent);
            Assert.AreEqual(boyWithGoodMood.GetMood(), Mood.Good);
            Assert.AreEqual(boyWithNeutralMood.GetMood(), Mood.Neutral);
            Assert.AreEqual(boyWithBadMood.GetMood(), Mood.Bad);
            Assert.AreEqual(boyWithHorribleMood.GetMood(), Mood.Horrible);
        }
        #endregion
    }
}