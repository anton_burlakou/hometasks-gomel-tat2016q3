﻿using NUnit.Framework;

namespace Nunit_testing_framework.Test
{
    [TestFixture]
    public class GirlTest
    {
        private const double Richness = 100000000;
        private const double Poor = 10000;
        #region wealth
        private static Girl[] richBoy =
        {
            new Girl(true, true, new Boy(Month.August, Richness))
        };

        private static Girl[] poorBoy =
        {
            new Girl(true, true, new Boy(Month.August, Poor)),
            new Girl(true, true, new Boy(Month.April))
        };
        #endregion

        #region wealth2
        private static Girl[] richBoy2 =
        {
            new Girl(true, true, new Boy(Month.August, Richness + 1000))
        };
        #endregion

        #region weight
        private static Girl[] slimFriend =
        {
            new Girl(true, false, new Boy(Month.August))
        };

        private static Girl[] fatFriend =
        {
            new Girl(true, true, new Boy(Month.August, Poor)),
            new Girl(true)
        };
        #endregion

        #region purchase
        private static Girl[] confirmPurchase =
        {
            new Girl(true, true, new Boy(Month.June, Richness))
        };

        private static Girl[] unconfirmPurchase =
        {
            new Girl(false, true, new Boy(Month.June, Richness)),
            new Girl(true, true, new Boy(Month.June, Poor)),
            new Girl(false, true, new Boy(Month.June, Poor))
        };
        #endregion

        private Boy boyFriend = new Boy(Month.August, Richness);

        private Girl girlWithExcelentMood;
        private Girl girlWithGoodMoodOne;
        private Girl girlWithGoodMoodTwo;
        private Girl girlWithNeutralMood;
        private Girl girlWithHateMood;

        public static Girl[] RichBoyProp { get; set; }

        public static Girl[] RichBoy2Prop { get; set; }

        public static Girl[] PoorBoyProp { get; set; }

        public static Girl[] SlimFriendBoyProp { get; set; }

        public static Girl[] FatFriendBoyProp { get; set; }

        public static Girl[] ConfirmPurchaseProp { get; set; }

        public static Girl[] UnconfirmPurchaseProp { get; set; }

        [SetUp]
        public void SetUp()
        {
            RichBoyProp = richBoy;
            RichBoy2Prop = richBoy2;
            PoorBoyProp = poorBoy;
            SlimFriendBoyProp = slimFriend;
            FatFriendBoyProp = fatFriend;
            ConfirmPurchaseProp = confirmPurchase;
            UnconfirmPurchaseProp = unconfirmPurchase;
            girlWithExcelentMood = new Girl(true, true, boyFriend);
            girlWithGoodMoodOne = new Girl(true);
            girlWithGoodMoodTwo = new Girl(false, false, new Boy(Month.April, Richness));
            girlWithNeutralMood = new Girl(false, false, new Boy(Month.April));
            girlWithHateMood = new Girl(false);
        }

        #region test wealth
        [Test, TestCaseSource("richBoy")]
        public void Affluent(Girl girl)
        {
            Assert.True(girl.IsBoyFriendRich());
        }

        [Test, TestCaseSource("poorBoy")]
        public void Indigent(Girl girl)
        {
            Assert.False(girl.IsBoyFriendRich());
        }
        #endregion

        #region test spendMoney
        [Test, TestCaseSource("richBoy2")]
        public void PurchaseShoes(Girl girl)
        {
            girl.SpendBoyFriendMoney(1000);
            Assert.AreEqual(girl.BoyFriend.Wealth, Richness);
        }

        [Test, TestCaseSource("poorBoy")]
        public void NotPurchaseShoes(Girl girl)
        {
            var currentWealth = girl.BoyFriend.Wealth;
            girl.SpendBoyFriendMoney(1000);
            Assert.AreEqual(girl.BoyFriend.Wealth, currentWealth);
        }
        #endregion

        #region test weight
        [Test, TestCaseSource("slimFriend")]
        public void Slim(Girl girl)
        {
            Assert.True(girl.IsSlimFriendBecameFat());
        }

        [Test, TestCaseSource("fatFriend")]
        public void Fat(Girl girl)
        {
            Assert.False(girl.IsSlimFriendBecameFat());
        }
        #endregion

        #region test Purchase
        [Test, TestCaseSource("confirmPurchase")]
        public void ConfirmPurchase(Girl girl)
        {
           Assert.True(girl.IsBoyFriendWillBuyNewShoes());
        }

        [Test, TestCaseSource("unconfirmPurchase")]
        public void UnconfirmPurchase(Girl girl)
        {
            Assert.False(girl.IsBoyFriendWillBuyNewShoes());
        }
        #endregion

        #region Mood
        [Test]
        public void GetMood()
        {
            Assert.AreEqual(girlWithExcelentMood.GetMood(), Mood.Excellent);
            Assert.AreEqual(girlWithGoodMoodOne.GetMood(), Mood.Good);
            Assert.AreEqual(girlWithGoodMoodTwo.GetMood(), Mood.Good);
            Assert.AreEqual(girlWithNeutralMood.GetMood(), Mood.Neutral);
            Assert.AreEqual(girlWithHateMood.GetMood(), Mood.IHateThemAll);
        }
        #endregion
    }
}
