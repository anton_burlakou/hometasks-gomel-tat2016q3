﻿namespace Nunit_testing_framework
{
    public abstract class Human
    {
        public abstract Mood GetMood();
    }
}