﻿//-----------------------------------------------------------------------
// <copyright file="MainContextMailPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Browser;
using FrameworkLayer.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using AutoItX3Lib;
using OpenQA.Selenium.Support.UI;
using System;
using FrameworkLayer.Screen;

namespace FrameworkLayer.MailRu.POM
{
    public class MainContextMailPage : IScreen
    {
        private BrowserInstance browser;

        #pragma warning disable 0649
        [FindsBy(How = How.XPath, Using = "//div[@id='b-compose']//textarea[@data-original-name='To']")]
        private IWebElement to;

        [FindsBy(How = How.XPath, Using = "//div[@id='b-compose']//input[@name='Subject']")]
        private IWebElement topic;

        [FindsBy(How = How.XPath, Using = "//div[@id='b-compose']//iframe")]
        private IWebElement iframe;

        [FindsBy(How = How.XPath, Using = "//div[@data-mnemo='attaches']//div[contains(@class, 'js-input-file')]//input")]
        private IWebElement attachFile;

        [FindsBy(How = How.Id, Using = "tinymce")]
        private IWebElement textbox;

        [FindsBy(How = How.XPath, Using = "//div[@id='b-toolbar__right']/div[not(contains(@style, 'display: none'))]//div[@data-name='send']")]
        private IWebElement send;

        [FindsBy(How = How.XPath, Using = "//div[@id='b-toolbar__right']/div[not(contains(@style, 'display: none'))]//div[@data-group='save-more']")]
        private IWebElement dropdownSaveAs;

        [FindsBy(How = How.XPath, Using = "//div[@id='b-toolbar__right']/div[not(contains(@style, 'display: none'))]//div[@data-name='remove']")]
        private IWebElement removeMail;
        #pragma warning restore 0649

        private By popupEmptyMail = By.XPath("//div[@id='MailRuConfirm']//div[contains(@class, 'is-compose-empty_in')]//button[@type='submit']");

        private By asTemplate = By.XPath("//div[@id='b-toolbar__right']/div[not(contains(@style, 'display: none'))]//a[@data-name='saveDraft']");

        public MainContextMailPage()
        {
            this.browser = new BrowserInstance();
            this.browser.InitElements(this);
        }

        public void FillTheMail(Mail mail)
        {
            this.IsNotNullThenFill(this.to, mail.To);
            this.IsNotNullThenFill(this.topic, mail.Topic);
            this.IsNotNullThenFill(this.attachFile, mail.PathToFile);
            /*
                ((IJavaScriptExecutor)this.driver).ExecuteScript("return document.getElementsByClassName('js-input-file')[0].getElementsByTagName('input')[0].classList = ''");
                this.attachFile.Click();
                AutoItX3 autoIt = new AutoItX3();
                autoIt.WinActivate("File Upload");
                autoIt.Send(mail.PathToFile);
                autoIt.Send("{ENTER}");
            */
            this.browser.Frame(this.iframe);
            this.IsNotNullThenFill(this.textbox, mail.TextOfMail);
            this.browser.SwitchToDefaultContent();
        }

        public void SendMail(Mail mail)
        {
            this.FillTheMail(mail);
            this.browser.Click(this.send);

            if (mail.Topic == null && mail.TextOfMail == null)
            {
                this.browser.Click(this.popupEmptyMail);
            }
        }

        public void FillTheMailAndSaveAsTemplate(Mail mail)
        {
            this.FillTheMail(mail);
            this.browser.Click(this.dropdownSaveAs);
            this.browser.Click(this.asTemplate);
        }

        public bool IsAlertExist()
        {
            var alert = this.browser.Alert();
            if (alert.Text != string.Empty)
            {
                return true;
            }

            return false;
        }

        public void RemoveMail()
        {
            this.browser.Click(this.removeMail);
        }

        private void IsNotNullThenFill(IWebElement element, string field)
        {
            if (field != null && field != string.Empty)
            {
                this.browser.SendKeys(element, field);
            }
        }
    }
}
