﻿//-----------------------------------------------------------------------
// <copyright file="LoginPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Browser;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using FrameworkLayer.Models;
using FrameworkLayer.POM;
using FrameworkLayer.Screen;

namespace FrameworkLayer.MailRu.POM
{
    public class LoginPage : ILoginPage, IScreen
    {
        private readonly string baseUrl;

        private BrowserInstance browser;

        #pragma warning disable 0649
        [FindsBy(How = How.Id, Using = "mailbox__login")]
        private IWebElement inputLogin;

        [FindsBy(How = How.Id, Using = "mailbox__password")]
        private IWebElement inputPassword;

        [FindsBy(How = How.Id, Using = "mailbox__auth__button")]
        private IWebElement buttonSubmit;
        #pragma warning restore 0649

        public LoginPage()
        {
            this.baseUrl = System.Configuration.ConfigurationManager.AppSettings["StartPageMail"];
            this.browser = new BrowserInstance();
            this.browser.InitElements(this);
        }

        public void OpenPage()
        {
            this.browser.OpenPage(this.baseUrl);
        }

        public void Login(Account account)
        {
            this.browser.SendKeys(this.inputLogin, account.Login);
            this.browser.SendKeys(this.inputPassword, account.Password);
            this.browser.Click(this.buttonSubmit);
        }
    }
}
