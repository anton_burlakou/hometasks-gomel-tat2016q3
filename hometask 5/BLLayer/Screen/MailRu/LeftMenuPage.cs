﻿//-----------------------------------------------------------------------
// <copyright file="LeftMenuPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Browser;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using FrameworkLayer.Screen;

namespace FrameworkLayer.MailRu.POM
{
    public class LeftMenuPage : IScreen
    {
        private BrowserInstance browser;

        private By writeMail = By.XPath("//div[@id='b-toolbar__left']//a[@data-bem='b-toolbar__btn']");

        private By sentBox = By.XPath("//div[@id='b-nav_folders']//a[@href='/messages/sent/']");

        private By inbox = By.XPath("//div[@id='b-nav_folders']//a[@href='/messages/inbox/']");

        private By draft = By.XPath("//div[@id='b-nav_folders']//a[@href='/messages/drafts/']");

        private By trash = By.XPath("//div[@id='b-nav_folders']//a[@href='/messages/trash/']");

        public LeftMenuPage()
        {
            this.browser = new BrowserInstance();
        }

        public void ClickButtonWriteMail()
        {
            this.Click(this.writeMail);
        }

        public void ClickLinkSendbox()
        {
            this.Click(this.sentBox);
        }

        public void ClickLinkInbox()
        {
            this.Click(this.inbox);
        }

        public void ClickLinkDraft()
        {
            this.Click(this.draft);
        }

        public void ClickLinkTrash()
        {
            this.Click(this.trash);
        }

        private void Click(By el)
        {
            this.browser.Click(el);
        }
    }
}
