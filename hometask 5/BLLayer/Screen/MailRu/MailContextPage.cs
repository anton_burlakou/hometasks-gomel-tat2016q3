﻿//-----------------------------------------------------------------------
// <copyright file="MailContextPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Browser;
using FrameworkLayer.Models;
using OpenQA.Selenium;
using System;
using System.Text.RegularExpressions;
using FrameworkLayer.Screen;

namespace FrameworkLayer.MailRu.POM
{
    public class MailContextPage : IScreen
    {
        private BrowserInstance browser;

        private By mailbox = By.XPath("//div[@id='b-letters']/div[1]/div[not(contains(@style, 'display: none'))]//div[@data-bem='b-datalist__item']");

        private By checkboxMail = By.ClassName("b-checkbox__box");

        private string pattern = @"<Без темы>";

        public MailContextPage()
        {
            this.browser = new BrowserInstance();
        }

        public bool CheckMail(Mail mail)
        {
            var now = DateTime.Now;
            bool check = true;
            while (DateTime.Now - now < TimeSpan.FromMinutes(2))
            {
                var element = this.browser.FindElement(this.mailbox);
                if (element.Displayed)
                {
                    IWebElement topicElement;
                    if (mail.Topic == null)
                    {
                        topicElement = this.browser.FindElementIntoCtx(element, By.ClassName("b-datalist__item__subj"));
                        TimeSpan dateTimeOfMail = TimeSpan.Parse(this.browser.FindElementIntoCtx(element, By.ClassName("b-datalist__item__date__value")).Text);
                        //// not correct time for later mail
                        if (Regex.IsMatch(topicElement.Text, this.pattern) && dateTimeOfMail.Hours == mail.Timestamp.Hours && dateTimeOfMail.Minutes == mail.Timestamp.Minutes)
                        {
                            check = true;
                        }
                        else
                        {
                            check = false;
                        }
                    }
                    else
                    {
                        By topicBy = By.XPath(string.Format("//*[contains(text(), {0})]", mail.Topic));
                        topicElement = this.browser.FindElementIntoCtx(element, topicBy);
                    }

                    if (check)
                    {
                        break;
                    }

                    this.browser.OpenPage(this.browser.GetUrl());
                }
            }

            return check;
        }

        public bool IsSearchMail(Mail mail)
        {
            try
            {
                var element = this.browser.FindElementIntoCtx(
                    this.mailbox,
                    By.XPath(string.Format("//*[contains(text(), {0})]", mail.Topic)),
                    true);
                if (element.Displayed)
                {
                    this.browser.FindElementIntoCtxAndClick(
                        this.mailbox,
                        this.checkboxMail,
                        false);
                    return true;
                }
            }
            catch
            {
                return false;
            }

            return false;
        }
    }
}
