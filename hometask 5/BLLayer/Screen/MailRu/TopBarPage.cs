﻿//-----------------------------------------------------------------------
// <copyright file="TopBarPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using OpenQA.Selenium;
using FrameworkLayer.Browser;
using FrameworkLayer.Screen;

namespace FrameworkLayer.MailRu.POM
{
    public class TopBarPage : IScreen
    {
        private BrowserInstance browser;

        private By greeting = By.Id("PH_user-email");

        public TopBarPage()
        {
            this.browser = new BrowserInstance();
        }

        public bool IsLogged()
        {
            var element = this.browser.FindElement(this.greeting);
            if (element != null && element.Displayed)
            {
                return true;
            }

            return false;
        }
    }
}
