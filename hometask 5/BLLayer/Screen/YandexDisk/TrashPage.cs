﻿// <copyright file="TrashPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using FrameworkLayer.Browser;
using FrameworkLayer.Screen;

namespace FrameworkLayer.POM.YandexDisk
{
    public class TrashPage : IScreen
    {
        private BrowserInstance browser;

        private By restoreBy = By.XPath("//button[@data-click-action='resource.restore']");

        private By contentBy = By.XPath("//div[@data-key='box=boxListing']");

        private By removeAllBy = By.XPath("//div[contains(@class, 'toolset__buttons')]//button[contains(@class, 'button_js_inited')]");

        public TrashPage()
        {
            this.browser = new BrowserInstance();
        }

        public bool IsExist(string path)
        {
            try
            {
                var file = this.SearchElement(path);
                if (file.Displayed)
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }

            return false;
        }

        public bool IsExist(List<string> filepathList)
        {
            var count = filepathList.Count;
            for (var i = 0; i < count; i++)
            {
                var check = this.IsExist(filepathList[i]);
                if (!check)
                {
                    break;
                }

                if (i == count - 1 && check)
                {
                    return true;
                }
            }

            return false;
        }

        public void Restore(string path)
        {
            var file = this.SearchElement(path);
            if (file.Displayed)
            {
                this.browser.Click(file);
                this.browser.Click(this.restoreBy);
            }
        }

        public void RemoveAll()
        {
            this.browser.Click(this.removeAllBy);
        }

        public void BackPage(string url)
        {
            this.browser.OpenPage(url);
        }

        private IWebElement SearchElement(string path)
        {
           By file = By.XPath(string.Format("//div[contains(text(), '{0}')]", System.IO.Path.GetFileName(path)));
           return this.browser.FindElementIntoCtx(this.contentBy, file, true);
        }
    }
}
