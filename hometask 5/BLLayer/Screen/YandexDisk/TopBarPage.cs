﻿// <copyright file="TopBarPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using FrameworkLayer.Browser;
using FrameworkLayer.Utils;
using FrameworkLayer.Screen;

namespace FrameworkLayer.POM.YandexDisk
{
    public class TopBarPage : IScreen
    {
        private BrowserInstance browser;

        private By attachFile = By.XPath("//input[contains(@class, 'button__attach')]");

        public TopBarPage()
        {
            this.browser = new BrowserInstance();
        }

        public void Upload(string path)
        {
            this.browser.SendKeys(this.attachFile, path, true);
        }
    }
}
