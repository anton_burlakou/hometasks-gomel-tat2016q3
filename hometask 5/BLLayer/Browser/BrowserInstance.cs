﻿// <copyright file="BrowserInstance.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using FrameworkLayer.WebElement;
using OpenQA.Selenium.Support.PageObjects;
using FrameworkLayer.Screen;

namespace FrameworkLayer.Browser
{
    public class BrowserInstance
    {
        private IWebDriver driver;
        private IMyWebElement myWebElement;

        public BrowserInstance()
        {
            this.driver = DriverInstance.GetInstance();
            this.myWebElement = new MyWebElement();
        }

        public void InitElements(IScreen screen)
        {
            PageFactory.InitElements(this.driver, screen);
        }

        public void OpenPage(string url)
        {
            this.driver.Navigate().GoToUrl(url);
        }

        public string GetUrl()
        {
            return this.driver.Url;
        }

        public IWebElement FindElementIntoCtx(IWebElement element, By elIntoCtx)
        {
            return element.FindElementIntoElementWithTimeOut(elIntoCtx);
        }

        public IWebElement FindElement(By element)
        {
            return this.driver.FindElementWithTimeOutDefault(element);
        }

        public IWebElement FindElement(By element, bool hidden = false)
        {
            return this.driver.FindElementWithTimeOutDefault(element, hidden);
        }

        public IWebElement FindElementIntoCtx(By ctx, By elIntoCtx, bool hiddenCtx = false)
        {
            var context = this.driver.FindElementWithTimeOutDefault(ctx, hiddenCtx);
            return context.FindElementIntoElementWithTimeOut(elIntoCtx);
        }

        public void Click(By element)
        {
            this.driver.FindElementWithTimeOutDefault(element).Click();
        }

        public void Click(IWebElement element)
        {
            element.Click();
        }

        public void Click(By element, bool hidden = false)
        {
            this.FindElement(element, hidden).Click();
        }

        public void FindElementIntoCtxAndClick(By ctx, By elIntoCtx, bool hiddenCtx = false)
        {
            this.FindElementIntoCtx(ctx, elIntoCtx, hiddenCtx).Click();
        }

        public void DragAndDrop(By source, By target)
        {
            this.myWebElement.DragAndDrop(
                this.driver.FindElementWithTimeOutDefault(source),
                this.driver.FindElementWithTimeOutDefault(target));
        }

        public void DragAndDrop(By ctxBy, By elIntoCtxBy, By targetBy)
        {
            var ctx = this.driver.FindElementWithTimeOutDefault(ctxBy);
            var elcontext = ctx.FindElementIntoElementWithTimeOut(elIntoCtxBy);
            this.myWebElement.DragAndDrop(
                elcontext,
                this.driver.FindElementWithTimeOutDefault(targetBy));
        }

        public void DoubleClick(By element)
        {
            this.myWebElement.DoubleClick(this.driver.FindElementWithTimeOutDefault(element));
        }

        public void CtrlClickElements(By ctx, By target, List<string> listEls)
        {
            var contextEl = this.driver.FindElementWithTimeOutDefault(ctx);
            var targetEl = this.driver.FindElementWithTimeOutDefault(target);
            this.myWebElement.CtrlClickElements(contextEl, targetEl, listEls);
        }

        public void CtrlClickElements(IWebElement ctx, IWebElement target, List<string> listEls)
        {
            this.myWebElement.CtrlClickElements(ctx, target, listEls);
        }

        public void SendKeys(IWebElement element, string text)
        {
            if (element.Displayed)
            {
                element.SendKeys(text);
            }
            else
            {
                // throw exception or logger
            }
        }

        public void SendKeys(By element, string text)
        {
            this.SendKeys(this.driver.FindElementWithTimeOutDefault(element), text);
        }

        public void SendKeys(By element, string text, bool hidden = false)
        {
            if (hidden)
            {
                this.driver.FindElementWithTimeOutDefault(element, hidden).SendKeys(text);
            }
            else
            {
                this.SendKeys(element, text);
            }
        }

        public IAlert Alert()
        {
            var wait = new OpenQA.Selenium.Support.UI.WebDriverWait(this.driver, TimeSpan.FromSeconds(10));
            wait.Until(d => d.SwitchTo().Alert());

            return this.driver.SwitchTo().Alert();
        }

        public void Frame(IWebElement element)
        {
            this.driver.SwitchTo().Frame(element);
        }

        public void SwitchToDefaultContent()
        {
            this.driver.SwitchTo().DefaultContent();
        }
    }
}
