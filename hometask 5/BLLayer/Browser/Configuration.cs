﻿//-----------------------------------------------------------------------
// <copyright file="Configuration.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------
namespace FrameworkLayer.Browser
{
    public class Configuration
    {
        public static string GetProjectConfig(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }

        public static BrowserType GetTypeFromConfig()
        {
            switch (GetProjectConfig("Browser"))
            {
                case "FireFox":
                    return BrowserType.Firefox;
                default:
                    return BrowserType.Chrome;
            }
        }
    }
}
