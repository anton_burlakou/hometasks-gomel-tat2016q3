﻿//-----------------------------------------------------------------------
// <copyright file="DriverInstance.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace FrameworkLayer.Browser
{
    public class DriverInstance
    {
        private static IWebDriver driver;

        private DriverInstance()
        {
        }

        public static IWebDriver GetInstance()
        {
            if (driver == null)
            {
                driver = CreateDriver();
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(60));
                driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(10));
                driver.Manage().Window.Maximize();
            }

            return driver;
        }

        public static void CloseBrowser()
        {
            driver.Quit();
            driver = null;
        }

        public static IWebDriver CreateDriver()
        {
            switch (Configuration.GetTypeFromConfig())
            {
                case BrowserType.Firefox:
                    var profile = new FirefoxProfile();
                    profile.SetPreference("browser.download.folderList", 2);
                    profile.SetPreference("browser.download.dir", System.Configuration.ConfigurationManager.AppSettings["FolderForDownload"]);
                    profile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
                    driver = new FirefoxDriver(profile);
                    break;
                default:
                    var pathToChromeBinary = System.Configuration.ConfigurationManager.AppSettings["chromePath"];
                    pathToChromeBinary = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../" + pathToChromeBinary);
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", System.Configuration.ConfigurationManager.AppSettings["FolderForDownload"]);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");
                    driver = new ChromeDriver(pathToChromeBinary, chromeOptions);
                    break;
            }

            return driver;
        }
    }
}
