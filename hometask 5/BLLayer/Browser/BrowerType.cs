﻿//-----------------------------------------------------------------------
// <copyright file="BrowerType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

namespace FrameworkLayer.Browser
{
    public enum BrowserType
    {
        Chrome,
        Firefox
    }
}
