﻿// <copyright file="IFileService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Service
{
    public interface IFileService
    {
        void UploadFile();

        List<bool> UploadFile(bool multiupload);

        bool IsFileUpload();

        bool IsFileUpload(List<bool> checklistFiles);

        void Download();

        bool IsCorrectFile();

        void MovedToTrash();

        void MovedToTrash(bool multiupload);

        bool IsMovedToTrash();

        bool IsMovedToTrash(bool multiupload);

        void RemoveFile();

        bool IsFileRemove();

        void Restore();

        bool IsRestore();
    }
}
