﻿//-----------------------------------------------------------------------
// <copyright file="BrowserService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Browser;

namespace FrameworkLayer.Service
{
    public class BrowserService : IBrowserService
    {
        public void Open()
        {
            DriverInstance.GetInstance();
        }

        public void Quit()
        {
            DriverInstance.CloseBrowser();
        }
    }
}
