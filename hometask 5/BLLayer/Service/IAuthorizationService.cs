﻿//-----------------------------------------------------------------------
// <copyright file="IAuthorizationService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------
using FrameworkLayer.Models;

namespace FrameworkLayer.Service
{
    public interface IAuthorizationService
    {
        void CorrectLogin(Models.Service service = Models.Service.Mail);

        void IncorrectLogin(Models.Service service = Models.Service.Mail);

        bool IsLoggedIn();
    }
}
