﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnitLite;

namespace TestLayer
{
    public static class Console
    {
        public static int Main(string[] args)
        {
            return new AutoRun().Execute(args);
        }
    }
}
