﻿// <copyright file="Console.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Collections.Generic;
using NUnitLite;
using NUnit.Common;
using System.Text;

namespace TestLayer
{
    public static class Console
    {
        public static int Main(string[] args)
        {
            var configBuilder = new ConfigureBuilder();
            ConfigStore.SetConfigure(configBuilder.CreateConfigureTest(ParserConfig.Parse(args)));
            FrameworkLayer.Browser.ConfigStore.SetConfigure(ConfigStore.GetConfigureDTO());
            var writer = new ExtendedTextWrapper(System.Console.Out);
            var numberOfThread = (ConfigStore.GetConfigure().Thread != null) ? ConfigStore.GetConfigure().Thread : "1";
            var detailConfig = new List<string>()
            {
                string.Format("--workers={0}", numberOfThread)
            }.ToArray();
            return new AutoRun().Execute(detailConfig);
        }
    }
}
