﻿// <copyright file="ConfigStore.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Browser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLayer
{
    public static class ConfigStore
    {
        private static ConfigureTest store;

        static ConfigStore()
        {
            ////store = new ConfigureTest();
        }

        public static void SetConfigure(ConfigureTest config)
        {
            store = config;
        }

        public static ConfigureTest GetConfigure()
        {
            return store;
        }

        public static Configuration GetConfigureDTO()
        {
            return new Configuration(store.Host, store.Port, store.Browser);
        }
    }
}
