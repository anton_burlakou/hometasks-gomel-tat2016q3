﻿//-----------------------------------------------------------------------
// <copyright file="Steps.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Service;
using FrameworkLayer.Models;
using System;
using NUnit.Framework;

namespace TestLayer.Steps
{
    public class Steps : ISteps
    {
        private IAuthorizationService authService;

        private IBrowserService browserService;

        private ISenderMailService senderService;

        private IFileService fileService;

        private IReportService reportService;

        public Steps()
        {
            this.browserService = new BrowserService();
            this.senderService = new SenderMailService();
            this.fileService = new FileService();
            this.reportService = new ReportService();
        }

        public void Login(Service service = Service.Mail, bool correctCredential = true)
        {
            this.authService = new AuthorizationService(service);
            if (correctCredential)
            {
                this.authService.CorrectLogin(service);
            }
            else
            {
                this.authService.IncorrectLogin(service);
            }
        }

        public void WriteMail(MailParams config = MailParams.FullWithCorrectMail)
        {
            if (config != MailParams.FullWithCorrectMail)
            {
                this.senderService = new SenderMailService(config);
            }

            this.senderService.ClickButtonWriteMail();
            this.senderService.FillTheMail();
        }

        public bool IsMailExist()
        {
            return this.senderService.IsMailExist();
        }

        public bool IsLoggedIn(Service service = Service.Mail)
        {
            this.authService = new AuthorizationService(service);
            return this.authService.IsLoggedIn();
        }

        public void InitBrowser()
        {
            this.browserService.Open();
        }

        public void CloseBrowser()
        {
            this.browserService.Quit();
        }

        public bool IsAlertExist()
        {
            return this.senderService.IsAlertExist();
        }

        public void CreateMailAsTemplate()
        {
            this.senderService.ClickButtonWriteMail();
            this.senderService.CreateMailAsTemplate();
        }

        public bool IsMailDeleted()
        {
            return this.senderService.IsMailDeleted();
        }

        public void UploadFile()
        {
            this.fileService.UploadFile();
            Assert.True(this.fileService.IsFileUpload(), "file is upload?");
        }

        public void UploadFiles()
        {
            var checklistFilesUpload = this.fileService.UploadFile(true);
            Assert.True(this.fileService.IsFileUpload(checklistFilesUpload), "all files is uploaded?");
        }

        public void Download()
        {
            this.fileService.Download();
            Assert.True(this.fileService.IsCorrectFile(), "file is correct");
        }

        public void MovedFileToTrash()
        {
            this.fileService.MovedToTrash();
            Assert.True(this.fileService.IsMovedToTrash(), "file moved to trash");
        }

        public void RemoveFile()
        {
            this.fileService.RemoveFile();
            Assert.False(this.fileService.IsFileRemove(), "file is remove");
        }

        public void RestoreFile()
        {
            this.fileService.Restore();
            Assert.True(this.fileService.IsRestore(), "file is restore");
        }

        public void MovedFilesToTrash()
        {
            this.fileService.MovedToTrash(true);
            Assert.True(this.fileService.IsMovedToTrash(true), "files moved to trash");
        }

        public void StartReport()
        {
            this.reportService.Start();
        }

        public void EndReport()
        {
            this.reportService.Finish();
        }

        public void GetReportResult(Error logstatus, string errorMsg)
        {
            this.reportService.GetReportResult(logstatus, errorMsg);
        }

        public void StartTest(string name)
        {
            this.reportService.StartTest(name);
        }
    }
}
