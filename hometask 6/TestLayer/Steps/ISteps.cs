﻿//-----------------------------------------------------------------------
// <copyright file="ISteps.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Models;
using System.Collections.Generic;

namespace TestLayer.Steps
{
    public interface ISteps
    {
        void Login(Service service = Service.Mail, bool correctCredential = true);

        void WriteMail(MailParams config = MailParams.FullWithCorrectMail);

        bool IsLoggedIn(Service service = Service.Mail);

        bool IsMailExist();

        bool IsAlertExist();

        bool IsMailDeleted();

        void InitBrowser();

        void CloseBrowser();

        void CreateMailAsTemplate();

        void UploadFile();

        void UploadFiles();

        void Download();

        void RemoveFile();

        void MovedFileToTrash();

        void MovedFilesToTrash();

        void RestoreFile();

        void StartReport();

        void EndReport();

        void GetReportResult(Error logstatus, string errorMsg);

        void StartTest(string name);
    }
}
