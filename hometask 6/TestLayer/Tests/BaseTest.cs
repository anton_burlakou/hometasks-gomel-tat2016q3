﻿//-----------------------------------------------------------------------
// <copyright file="BaseTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestLayer.Steps;
using FrameworkLayer.Models;

namespace TestLayer.Tests
{
    public class BaseTest
    {
        public ISteps step = new Steps.Steps();

        [OneTimeSetUp]
        public void StartReport()
        {
            var parameters = TestContext.Parameters;
            var dictParams = new Dictionary<string, string>();
            if (parameters.Count > 0 && ConfigStore.GetConfigure() == null)
            {
                foreach (var param in parameters.Names)
                {
                    dictParams.Add(param, parameters.Get(param));
                    System.Console.WriteLine("key - {0}, value - {1}", param, parameters.Get(param));
                }

                var configBuilder = new ConfigureBuilder();
                ConfigStore.SetConfigure(configBuilder.CreateConfigureTest(dictParams));
                FrameworkLayer.Browser.ConfigStore.SetConfigure(ConfigStore.GetConfigureDTO());
            }

            this.step.StartReport();
        }

        [SetUp]
        public void Init()
        {
            this.step.InitBrowser();
        }

        [TearDown]
        public void Dispose()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stackTrace = string.Format("<pre>{0}</pre>", TestContext.CurrentContext.Result.StackTrace);
            var errorMessage = TestContext.CurrentContext.Result.Message;

            Error logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Error.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Error.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Error.Skip;
                    break;
                default:
                    logstatus = Error.Pass;
                    break;
            }

            this.step.GetReportResult(logstatus, stackTrace + errorMessage);
            this.step.CloseBrowser();
        }

        [OneTimeTearDown]
        public void EndReport()
        {
            this.step.EndReport();
        }
    }
}
