﻿// <copyright file="UploadTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestLayer.Tests;
using NUnit.Framework;
using FrameworkLayer.Models;

namespace TestLayer.Tests.Yandex
{
    [TestFixture]
    public class UploadTest : BaseTest
    {
        [Test]
        public void Upload()
        {
            this.step.StartTest(this.GetType().Name);
            this.step.Login(Service.Yandex);
            this.step.UploadFile();
            this.step.Download();
        }

        [Test]
        public void UploadAndRemove()
        {
            this.step.StartTest(this.GetType().Name);
            this.step.Login(Service.Yandex);
            this.step.UploadFile();
            this.step.MovedFileToTrash();
            this.step.RemoveFile();
        }

        [Test]
        public void UploadAndMovedToTrashAndRestore()
        {
            this.step.StartTest(this.GetType().Name);
            this.step.Login(Service.Yandex);
            this.step.UploadFile();
            this.step.MovedFileToTrash();
            this.step.RestoreFile();
        }

        [Test]
        public void UploadAndRemoveSeveralFiles()
        {
            this.step.StartTest(this.GetType().Name);
            this.step.Login(Service.Yandex);
            this.step.UploadFiles();
            this.step.MovedFilesToTrash();
        }
    }
}
