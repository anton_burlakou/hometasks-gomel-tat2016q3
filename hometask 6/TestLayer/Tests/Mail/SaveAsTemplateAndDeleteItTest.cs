﻿//-----------------------------------------------------------------------
// <copyright file="SaveAsTemplateAndDeleteItTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestLayer.Tests;

namespace TestLayer.Mail.Tests
{
    [TestFixture]
    public class SaveAsTemplateAndDeleteItTest : BaseTest
    {
        [Test]
        public void SaveMailAsTemplateAndDeleteIt()
        {
            this.step.StartTest(this.GetType().Name);
            this.step.Login();
            this.step.CreateMailAsTemplate();
            Assert.True(this.step.IsMailDeleted(), "Mail is delete");
        }
    }
}
