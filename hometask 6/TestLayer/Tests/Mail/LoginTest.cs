﻿//-----------------------------------------------------------------------
// <copyright file="LoginTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FrameworkLayer.Service;
using TestLayer.Steps;
using FrameworkLayer.Models;
using TestLayer.Tests;

namespace TestLayer.Mail.Tests
{
    [TestFixture]
    public class LoginTest : BaseTest
    {
        [Test]
        public void LoginIntoService()
        {
            this.step.StartTest(this.GetType().Name);
            this.step.Login();
            Assert.True(this.step.IsLoggedIn(), "Test on positive login");
        }

        [Test]
        public void LoginIntoServiceWithIncorrectCredential()
        {
            this.step.StartTest(this.GetType().Name);
            this.step.Login(Service.Mail, false);
            Assert.False(this.step.IsLoggedIn(), "Test on negative login");
        }
    }
}
