﻿// <copyright file="ConfigureBuilder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Collections.Generic;

namespace TestLayer
{
    public class ConfigureBuilder
    {
        private ConfigureTest configure;

        public ConfigureBuilder()
        {
            this.configure = new ConfigureTest();
        }

        public ConfigureTest CreateConfigureTest(Dictionary<string, string> config)
        {
            this.ConfigureTest(config);
            return this.configure;
        }

        private ConfigureBuilder Host(string host)
        {
            this.configure.Host = host;
            return this;
        }

        private ConfigureBuilder Port(string port)
        {
            this.configure.Port = port;
            return this;
        }

        private ConfigureBuilder Browser(string browser)
        {
            this.configure.Browser = browser;
            return this;
        }

        private ConfigureBuilder Thread(string thread)
        {
            this.configure.Thread = thread;
            return this;
        }

        private bool ConfigureTest(Dictionary<string, string> config)
        {
            foreach (var keypair in config)
            {
                switch (keypair.Key)
                {
                    case "host":
                        this.Host(keypair.Value);
                        break;
                    case "port":
                        this.Port(keypair.Value);
                        break;
                    case "b":
                        this.Browser(keypair.Value);
                        break;
                    case "thread":
                        this.Thread(keypair.Value);
                        break;
                    default:
                        System.Console.WriteLine("List of commands: \n\\host - selenium grid host\n\\port - selenium port\n\\b - browser (Firefox or Chrome)\n\thread - number of worker for parallel run tests ");
                        break;
                }
            }

            return true;
        }
    }
}
