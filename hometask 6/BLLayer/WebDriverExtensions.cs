﻿//-----------------------------------------------------------------------
// <copyright file="WebDriverExtensions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------
using FrameworkLayer.LogService;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace FrameworkLayer
{
    public static class WebDriverExtensions
    {
        private static TimeSpan timeout = TimeSpan.FromSeconds(60);

        public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds, bool hidden = false)
        {
            if (timeoutInSeconds > 0)
            {
                var waitForPage = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                waitForPage.Until(d => d.Execute("return document.readyState").Equals("complete"));
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                try
                {
                    if (hidden)
                    {
                        wait.Until(d => d.FindElement(by));
                    }
                    else
                    {
                        wait.Until(d => d.FindElement(by).Displayed);
                    }

                    Logger.Info(string.Format("element {0} was found", by));

                    return driver.FindElement(by);
                }
                catch
                {
                    Logger.Error(string.Format("element {0} wasn't found", by));
                    return null;
                }
            }

            return driver.FindElement(by);
        }

        public static IWebElement FindElementWithTimeOutDefault(this IWebDriver driver, By by, bool hidden = false)
        {
            Thread.Sleep(1000);
                var waitForPage = new WebDriverWait(driver, timeout);
                waitForPage.Until(d => d.Execute("return document.readyState").Equals("complete"));

                var wait = new WebDriverWait(driver, timeout);
                try
                {
                    if (hidden)
                    {
                        wait.Until(d => d.FindElement(by));
                    }
                    else
                    {
                        wait.Until(d => d.FindElement(by).Displayed);
                    }

                Logger.Info(string.Format("element {0} was found", by));
                return driver.FindElement(by);
                }
                catch
                {
                Logger.Error(string.Format("element {0} wasn't found", by));
                return null;
                }
        }

        public static IWebElement FindElementIntoElementWithTimeOut(this ISearchContext context, By by)
        {
            var wait = new DefaultWait<ISearchContext>(context);
            wait.Timeout = timeout;
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait.Until(ctx =>
            {
                var elem = ctx.FindElement(by);
                if (!elem.Displayed)
                {
                    Logger.Error(string.Format("element {0} wasn't found", by));
                    return null;
                }
                Logger.Info(string.Format("element {0} was found", by));
                return elem;
            });
        }

        public static object Execute(this IWebDriver driver, string script)
        {
            return ((IJavaScriptExecutor)driver).ExecuteScript(script);
        }
    }
}
