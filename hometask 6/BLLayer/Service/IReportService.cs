﻿// <copyright file="IReportService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Models;

namespace FrameworkLayer.Service
{
    public interface IReportService
    {
        void Start();

        void Finish();

        void GetReportResult(Error logstatus, string errorMsg);

        void StartTest(string name);
    }
}
