﻿//-----------------------------------------------------------------------
// <copyright file="IBrowserService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

namespace FrameworkLayer.Service
{
    public interface IBrowserService
    {
        void Open();

        void Quit();
    }
}
