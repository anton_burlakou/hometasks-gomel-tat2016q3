﻿// <copyright file="ReportService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RelevantCodes.ExtentReports;
using FrameworkLayer.Models;
using FrameworkLayer.LogService;

namespace FrameworkLayer.Service
{
    public class ReportService : IReportService
    {
        private ExtentReports extent;
        private ExtentTest test;
        private Logger logger = new Logger();

        public void StartTest(string name)
        {
            this.test = this.extent.StartTest(name);
            Logger.Info(string.Format("Test name {0} started", name));
        }

        public void Finish()
        {
            this.extent.Flush();
            this.extent.Close();
        }

        public void GetReportResult(Error logstatus, string errorMsg)
        {
            LogStatus status;
            switch (logstatus)
            {
                case Error.Fail:
                    status = LogStatus.Fail;
                    break;
                case Error.Warning:
                    status = LogStatus.Warning;
                    break;
                case Error.Skip:
                    status = LogStatus.Skip;
                    break;
                default:
                    status = LogStatus.Pass;
                    break;
            }

            if (status == LogStatus.Fail)
            {
                var testFailed = string.Format("Test ended with {0} status and stackTrace {1} ", logstatus, errorMsg);
                this.test?.Log(status, testFailed);
                Logger.Error(testFailed);
            }
            else
            {
                var testPassed = string.Format("Test finished with status code {0}", status);
                this.test.Log(status, testPassed);
                Logger.Info(testPassed);
            }

            Logger.TakeScreenshot();
            Logger.Info("Test finished");
            this.extent.EndTest(this.test);
        }

        public void Start()
        {
            var path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../");
            this.extent = new ExtentReports(System.IO.Path.Combine(path, string.Format("Logs/Reports/report{0}.html", DateTime.Now.ToString("hh_mm_ss_tt"))), true);
            this.extent.AddSystemInfo("Host Name", "localhost")
                .AddSystemInfo("Environment", "QA")
                .AddSystemInfo("User Name", "Aburlakou");
            this.extent.LoadConfig(path + "extent-config.xml");
        }
    }
}
