﻿//-----------------------------------------------------------------------
// <copyright file="ISenderMailService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

namespace FrameworkLayer.Service
{
    public interface ISenderMailService
    {
        void ClickButtonWriteMail();

        void FillTheMail();

        bool IsMailExist();

        bool IsAlertExist();

        bool IsMailDeleted();

        void CreateMailAsTemplate();
    }
}
