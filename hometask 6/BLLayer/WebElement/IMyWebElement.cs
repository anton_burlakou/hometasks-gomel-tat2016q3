﻿// <copyright file="IMyWebElement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace FrameworkLayer.WebElement
{
    public interface IMyWebElement : IWebElement
    {
        void DragAndDrop(IWebElement source, IWebElement target);

        void DoubleClick(IWebElement source);

        void CtrlClickElements(IWebElement ctx, IWebElement target, List<string> listEls);
    }
}
