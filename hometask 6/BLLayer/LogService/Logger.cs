﻿// <copyright file="Logger.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using FrameworkLayer.Browser;

namespace FrameworkLayer.LogService
{
    public class Logger
    {
        private static NLog.Logger logger;

        public Logger()
        {
            logger = LogManager.GetCurrentClassLogger();
        }

        public Logger(string name)
        {
            logger = LogManager.GetLogger(name);
        }

        public static void Trace(string message)
        {
            logger.Trace(message);
        }

        public static void Debug(string message)
        {
            logger.Debug(message);
        }

        public static void Info(string message)
        {
            logger.Info(message);
        }

        public static void Warn(string message)
        {
            logger.Warn(message);
        }

        public static void Error(string message)
        {
            logger.Error(message);
        }

        public static void Fatal(string message)
        {
            logger.Fatal(message);
        }

        public static void TakeScreenshot()
        {
            DriverInstance.TakeScreenshot();
        }
    }
}
