﻿// <copyright file="EventHandlerLogger.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.Events;
using FrameworkLayer.Browser;
using OpenQA.Selenium;

namespace FrameworkLayer.LogService
{
    public class EventHandlerLogger : EventFiringWebDriver
    {
        private IWebDriver driver;

        public EventHandlerLogger(IWebDriver driver)
            : base(driver)
        {
        }

        protected override void OnElementClicking(WebElementEventArgs e)
        {
            Logger.TakeScreenshot();
            base.OnElementClicking(e);
            Logger.Info(string.Format(
                "Click by element {0} with text {1} and coordinates {2}",
                e.Element.TagName,
                e.Element.Text,
                e.Element.Location));
        }

        protected override void OnNavigating(WebDriverNavigationEventArgs e)
        {
            base.OnNavigating(e);
        }

        protected override void OnNavigatedBack(WebDriverNavigationEventArgs e)
        {
            base.OnNavigatedBack(e);
        }

        protected override void OnNavigatedForward(WebDriverNavigationEventArgs e)
        {
            base.OnNavigatedForward(e);
        }

        protected override void OnElementClicked(WebElementEventArgs e)
        {
            base.OnElementClicked(e);
            Logger.Info(string.Format("Clicked by element"));
        }

        protected override void OnFindElementCompleted(FindElementEventArgs e)
        {
            base.OnFindElementCompleted(e);
            Logger.Info(string.Format("Search current element by method {0} completed", e.FindMethod));
        }

        protected override void OnScriptExecuted(WebDriverScriptEventArgs e)
        {
            base.OnScriptExecuted(e);
            Logger.Info(string.Format("Script {0} executed", e.Script));
        }

        protected override void OnNavigated(WebDriverNavigationEventArgs e)
        {
            base.OnNavigated(e);
            Logger.Info(string.Format(" {0} page opened", e.Url));
        }

        protected override void OnFindingElement(FindElementEventArgs e)
        {
            base.OnFindingElement(e);
            Logger.Info(string.Format("Searching element - {0} finding", e.Element));
        }
    }
}
