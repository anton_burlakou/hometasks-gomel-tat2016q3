﻿// <copyright file="ILoginPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.POM
{
    public interface ILoginPage
    {
        void OpenPage();

        void Login(Account account);
    }
}
