﻿// <copyright file="UploadPopupPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Browser;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrameworkLayer.Screen;

namespace FrameworkLayer.POM.YandexDisk
{
    public class UploadPopupPage : IScreen
    {
        private BrowserInstance browser;

        private By popupTitle = By.CssSelector("div._nb-modal-popup div._nb-popup-title div");

        private By close = By.CssSelector("div._nb-modal-popup a[data-click-action='dialog.close']");

        public UploadPopupPage()
        {
            this.browser = new BrowserInstance();
        }

        public bool IsUpload()
        {
            var title = this.browser.FindElement(this.popupTitle);
            if (title.Displayed)
            {
                if (Equals(title.Text, "Загрузка завершена"))
                {
                         return true;
                }

                return false;
            }

            return false;
        }

        public void Close()
        {
            this.browser.Click(this.close);
        }
    }
}
