﻿// <copyright file="LoginPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using FrameworkLayer.POM;
using FrameworkLayer.Models;
using FrameworkLayer.Browser;
using OpenQA.Selenium.Support.PageObjects;
using FrameworkLayer.Screen;

namespace FrameworkLayer.POM.YandexDisk
{
    public class LoginPage : ILoginPage, IScreen
    {
        private readonly string baseUrl;

        private BrowserInstance browser;
        #pragma warning disable 0649
        [FindsBy(How = How.XPath, Using = "//input[@name='login']")]
        private IWebElement inputLogin;

        [FindsBy(How = How.XPath, Using = "//input[@name='passwd']")]
        private IWebElement inputPassword;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class, 'button auth__button domik2__auth-button')]")]
        private IWebElement buttonSubmit;
        #pragma warning restore 0649

        public LoginPage()
        {
            this.baseUrl = System.Configuration.ConfigurationManager.AppSettings["StartPageYandex"];
            this.browser = new BrowserInstance();
            this.browser.InitElements(this);
        }

        public void Login(Account account)
        {
            this.browser.SendKeys(this.inputLogin, account.Login);
            this.browser.SendKeys(this.inputPassword, account.Password);
            this.browser.Click(this.buttonSubmit);
            this.browser.Click(By.XPath("//a[@data-id='disk']"));
        }

        public void OpenPage()
        {
            this.browser.OpenPage(this.baseUrl);
        }
    }
}
