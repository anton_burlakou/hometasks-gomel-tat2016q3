﻿// <copyright file="ContentPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using FrameworkLayer.Browser;
using OpenQA.Selenium.Interactions;
using FrameworkLayer.WebElement;
using FrameworkLayer.Screen;

namespace FrameworkLayer.POM.YandexDisk
{
    public class ContentPage : IScreen
    {
        private BrowserInstance browser;

        ////div[contains(@class, 'b-listing__chunk')]
        private By content = By.XPath("//div[contains(@class, 'b-content')]//div[contains(@class,'ns-view-resources')]");

        private By dlButton = By.XPath("//button[@data-click-action='resource.download']");

        private By trashBy = By.XPath("//div[contains(@class, 'b-content')]//div[contains(@class,'ns-view-resources')]//div[@title='Корзина']");

        public ContentPage()
        {
            this.browser = new BrowserInstance();
        }

        public string GetCurrentUrl()
        {
           return this.browser.GetUrl();
        }

        public void Download(string path)
        {
            this.browser.FindElementIntoCtxAndClick(
                this.content,
                By.XPath(string.Format("//div[@title='{0}']", System.IO.Path.GetFileName(path))),
                true);
            this.browser.Click(this.dlButton);
        }

        public void MovedToTrash(string path)
        {
            this.browser.DragAndDrop(
                this.content,
                By.XPath(string.Format("//div[@title='{0}']", System.IO.Path.GetFileName(path))),
                this.trashBy);
        }

        public void MovedToTrashFilesWithCtrlClick(List<string> filepathList)
        {
            var filelist = filepathList.Select(f => string.Format("//div[@title='{0}']", System.IO.Path.GetFileName(f)));
            this.browser.CtrlClickElements(
                this.content,
                this.trashBy,
                filelist.ToList());
        }

        public void OpenTrash()
        {
            this.browser.DoubleClick(this.trashBy);
        }

        public bool IsRestore(string path)
        {
            try
            {
               var element = this.browser.FindElementIntoCtx(
                    this.content,
                    By.XPath(string.Format("//div[@title='{0}']", System.IO.Path.GetFileName(path))),
                    true);

                if (element.Displayed)
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }

            return false;
        }
    }
}
