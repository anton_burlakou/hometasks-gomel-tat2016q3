﻿// <copyright file="TrashPopupPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using OpenQA.Selenium;
using FrameworkLayer.Browser;
using FrameworkLayer.Screen;

namespace FrameworkLayer.POM.YandexDisk
{
    public class TrashPopupPage : IScreen
    {
        private BrowserInstance browser;

        private By confirmButtonBy = By.XPath("//div[contains(@class, '_nb-popup b-confirmation ')]//button[contains(@class, 'js-confirmation-accept')]");

        public TrashPopupPage()
        {
            this.browser = new BrowserInstance();
        }

        public void Confirm()
        {
            this.browser.Click(this.confirmButtonBy);
        }
    }
}
