﻿//-----------------------------------------------------------------------
// <copyright file="DriverInstance.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using FrameworkLayer.LogService;
using System.Diagnostics;

namespace FrameworkLayer.Browser
{
    public class DriverInstance
    {
        private const string Localhost = "http://localhost:4444/wd/hub";
        private static IWebDriver driver;
        private static Process proc;

        private DriverInstance()
        {
        }

        public static IWebDriver GetInstance()
        {
            if (driver == null)
            {
                driver = CreateDriver();
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(60));
                driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(10));
                driver.Manage().Window.Maximize();
            }

            return driver;
        }

        public static void CloseBrowser()
        {
            proc?.Close();
            driver.Quit();
            driver = null;
        }

        public static IWebDriver CreateDriver()
        {
            var config = ConfigStore.GetConfigure();
            if (config?.Host != null || config?.Port != null)
            {
                config.FullHost = GetHost();
                CreateStandAloneServer();
            }

                switch (Configuration.GetTypeFromConfig())
               {
                case BrowserType.Firefox:
                    var profileFireFox = new FirefoxProfile();
                    profileFireFox.SetPreference("browser.download.folderList", 2);
                    profileFireFox.SetPreference("browser.download.dir", System.Configuration.ConfigurationManager.AppSettings["FolderForDownload"]);
                    profileFireFox.SetPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");

                    if (config?.Host != null)
                    {
                        DesiredCapabilities capabilities = DesiredCapabilities.Firefox();
                        capabilities.SetCapability(FirefoxDriver.ProfileCapabilityName, profileFireFox.ToBase64String());
                        driver = new RemoteWebDriver(new Uri(config.FullHost), capabilities);
                    }
                    else
                    {
                        driver = new FirefoxDriver(profileFireFox);
                    }

                    break;
                default:
                    var pathToChromeBinary = System.Configuration.ConfigurationManager.AppSettings["chromePath"];
                    pathToChromeBinary = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../" + pathToChromeBinary);
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", System.Configuration.ConfigurationManager.AppSettings["FolderForDownload"]);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");

                    if (config?.Host != null)
                    {
                        DesiredCapabilities capabilities = DesiredCapabilities.Chrome();
                        capabilities.SetCapability("chrome.binary", pathToChromeBinary);
                        capabilities.SetCapability(ChromeOptions.Capability, chromeOptions);
                        driver = new RemoteWebDriver(new Uri(config.FullHost), capabilities);
                    }
                    else
                    {
                        driver = new ChromeDriver(pathToChromeBinary, chromeOptions);
                    }

                    break;
            }

            return new EventHandlerLogger(driver);
        }

        public static void TakeScreenshot()
        {
            string path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../Logs");
            if (driver != null)
            {
                Screenshot scr = ((ITakesScreenshot)driver).GetScreenshot();
                scr.SaveAsFile(
                    string.Format("{0}//{1}.png", path, DateTime.Now.ToString("hh_mm_ss_tt")),
                    System.Drawing.Imaging.ImageFormat.Png);
            }
        }

        private static string GetHost()
        {
            var config = ConfigStore.GetConfigure();
            string host = string.Empty;
            if (config.Host != null || config.Port != null)
            {
                if (config.Host != null)
                {
                    host = (config.Port == null) ?
                    string.Format("{0}/wd/hub", config.Host) :
                    string.Format("{0}:{1}/wd/hub", config.Host, config.Port);
                }
                else
                {
                    host = Localhost;
                    if (config.Port != null)
                    {
                        host.Replace("4444", config.Port);
                    }
                }
            }

            return host;
        }

        private static void CreateStandAloneServer()
        {
            var pathToDirectory = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../");
            var pathToSeleniumStandAloneServer = System.IO.Path.Combine(pathToDirectory, "selenium-server-standalone-2.53.0.jar ");
            var pathToDriverChrome = System.IO.Path.Combine(pathToDirectory, "../BLLayer/chromedriver.exe");
            var config = ConfigStore.GetConfigure();
            var process = new ProcessStartInfo("java.exe")
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                Arguments = string.Format(
                    "-jar {0} -hub {1}/ -port {2} -Dwebdriver.chrome.driver={3}",
                    pathToSeleniumStandAloneServer,
                    config.Host,
                    config.Port,
                pathToDriverChrome)
            };

            if ((proc = Process.Start(process)) == null)
            {
                throw new InvalidOperationException("Selenium Standalone isn`t runned!");
            }
        }
    }
}
