﻿//-----------------------------------------------------------------------
// <copyright file="ISteps.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLayer.Steps
{
    public interface ISteps
    {
        void Login(bool correctCredential = true);

        void WriteMail(MailParams config = MailParams.FullWithCorrectMail);

        bool IsLoggedIn();

        bool IsMailExist();

        bool IsAlertExist();

        bool IsMailDeleted();

        void InitBrowser();

        void CloseBrowser();

        void CreateMailAsTemplate();
    }
}
