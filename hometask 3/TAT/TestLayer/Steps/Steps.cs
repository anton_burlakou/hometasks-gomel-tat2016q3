﻿//-----------------------------------------------------------------------
// <copyright file="Steps.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Service;
using FrameworkLayer.Models;

namespace TestLayer.Steps
{
    public class Steps : ISteps
    {
        private IAuthorizationService authService;

        private IBrowserService browser;

        private ISenderMailService sender;

        public Steps()
        {
            this.authService = new AuthorizationService();
            this.browser = new BrowserService();
            this.sender = new SenderMailService();
        }

        public void Login(bool correctCredential = true)
        {
            if (correctCredential)
            {
                this.authService.CorrectLogin();
            }
            else
            {
                this.authService.IncorrectLogin();
            }
        }

        public void WriteMail(MailParams config = MailParams.FullWithCorrectMail)
        {
            if (config != MailParams.FullWithCorrectMail)
            {
                this.sender = new SenderMailService(config);
            }

            this.sender.ClickButtonWriteMail();
            this.sender.FillTheMail();
        }

        public bool IsMailExist()
        {
            return this.sender.IsMailExist();
        }

        public bool IsLoggedIn()
        {
            return this.authService.IsLoggedIn();
        }

        public void InitBrowser()
        {
            this.browser.Open();
        }

        public void CloseBrowser()
        {
            this.browser.Quit();
        }

        public bool IsAlertExist()
        {
            return this.sender.IsAlertExist();
        }

        public void CreateMailAsTemplate()
        {
            this.sender.ClickButtonWriteMail();
            this.sender.CreateMailAsTemplate();
        }

        public bool IsMailDeleted()
        {
            return this.sender.IsMailDeleted();
        }
    }
}
