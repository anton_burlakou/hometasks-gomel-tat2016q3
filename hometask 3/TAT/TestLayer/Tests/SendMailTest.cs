﻿//-----------------------------------------------------------------------
// <copyright file="SendMailTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FrameworkLayer.Models;

namespace TestLayer.Tests
{
    public class SendMailTest : BaseTest
    {
        [Test]
        public void SendMailWithFullData()
        {
            this.step.Login();
            this.step.WriteMail();
            Assert.True(this.step.IsMailExist(), "mail is exist");
        }

        [Test]
        public void SendMailWithoutMailAdress()
        {
            this.step.Login();
            this.step.WriteMail(MailParams.WithoutAdress);
            Assert.True(this.step.IsAlertExist(), "Alert is exist");
        }

        [Test]
        public void SendMailWithoutTopicAndBody()
        {
            this.step.Login();
            this.step.WriteMail(MailParams.WithoutSubjectAndBody);
            Assert.True(this.step.IsMailExist(), "mail is exist");
        }
    }
}
