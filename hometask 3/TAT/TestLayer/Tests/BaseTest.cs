﻿//-----------------------------------------------------------------------
// <copyright file="BaseTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestLayer.Steps;

namespace TestLayer.Tests
{
    public class BaseTest
    {
        public ISteps step = new Steps.Steps();

        [SetUp]
        public void Init()
        {
            this.step.InitBrowser();
        }

        [TearDown]
        public void Dispose()
        {
            this.step.CloseBrowser();
        }
    }
}
