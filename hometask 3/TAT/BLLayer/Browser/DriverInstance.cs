﻿//-----------------------------------------------------------------------
// <copyright file="DriverInstance.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace FrameworkLayer.Browser
{
    public class DriverInstance
    {
        private static IWebDriver driver;

        private DriverInstance()
        {
        }

        public static IWebDriver GetInstance()
        {
            if (driver == null)
            {
                driver = CreateDriver();
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(60));
                driver.Manage().Window.Maximize();
            }

            return driver;
        }

        public static void CloseBrowser()
        {
            driver.Quit();
            driver = null;
        }

        public static IWebDriver CreateDriver()
        {
            switch (Configuration.GetTypeFromConfig())
            {
                case BrowserType.Firefox:
                    driver = new FirefoxDriver();
                    break;
                default:
                    var path = System.Configuration.ConfigurationManager.AppSettings["chromePath"];
                    path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "../../../" + path);
                    driver = new ChromeDriver(path);
                    break;
            }

            return driver;
        }
    }
}
