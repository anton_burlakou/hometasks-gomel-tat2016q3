﻿//-----------------------------------------------------------------------
// <copyright file="AuthorizationService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Models;
using FrameworkLayer.POM;
using FrameworkLayer.Utils;

namespace FrameworkLayer.Service
{
    public class AuthorizationService : IAuthorizationService
    {
        private Account validAccount;

        private Account incorrectAccount;

       ////private LoginPage loginPage = new LoginPage();

        public AuthorizationService()
        {
            var login = System.Configuration.ConfigurationManager.AppSettings["Login"];
            var password = System.Configuration.ConfigurationManager.AppSettings["Password"];
            this.validAccount = new Account(login, password);

            var random = new System.Random();
            this.incorrectAccount = new Account(RandomGenerator.GetRandomString(random.Next(0, 10)), RandomGenerator.GetRandomString(random.Next(0, 10)));
        }

        public void CorrectLogin()
        {
            var loginPage = new LoginPage();
            loginPage.OpenPage();
            loginPage.Login(this.validAccount);
        }

        public void IncorrectLogin()
        {
            var loginPage = new LoginPage();
            loginPage.OpenPage();
            loginPage.Login(this.incorrectAccount);
        }

        public bool IsLoggedIn()
        {
            var topbar = new TopBarPage();
            return topbar.IsLogged();
        }
    }
}
