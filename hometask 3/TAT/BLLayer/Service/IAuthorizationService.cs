﻿//-----------------------------------------------------------------------
// <copyright file="IAuthorizationService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

namespace FrameworkLayer.Service
{
    public interface IAuthorizationService
    {
        void CorrectLogin();

        void IncorrectLogin();

        bool IsLoggedIn();
    }
}
