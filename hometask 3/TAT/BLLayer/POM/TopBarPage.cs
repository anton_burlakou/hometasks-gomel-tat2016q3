﻿//-----------------------------------------------------------------------
// <copyright file="TopBarPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using OpenQA.Selenium;
using FrameworkLayer.Browser;

namespace FrameworkLayer.POM
{
    public class TopBarPage
    {
        private IWebDriver driver;

        private By greeting = By.Id("PH_user-email");

        public TopBarPage()
        {
            this.driver = DriverInstance.GetInstance();
        }

        public bool IsLogged()
        {
            var element = this.driver.FindElement(this.greeting, 1);
            if (element.Displayed)
            {
                return true;
            }

            return false;
        }
    }
}
