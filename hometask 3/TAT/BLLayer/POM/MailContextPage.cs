﻿//-----------------------------------------------------------------------
// <copyright file="MailContextPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Browser;
using FrameworkLayer.Models;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FrameworkLayer.POM
{
    public class MailContextPage
    {
        private IWebDriver driver;

        private By mailbox = By.XPath("//div[@id='b-letters']/div[1]/div[not(contains(@style, 'display: none'))]//div[@data-bem='b-datalist__item']");

        private By checkboxMail = By.ClassName("b-checkbox__box");

        private string pattern = @"<Без темы>";

        public MailContextPage()
        {
            this.driver = DriverInstance.GetInstance();
        }

        public bool CheckMail(Mail mail)
        {
            var element = this.driver.FindElement(this.mailbox, 1);
            if (element.Displayed)
            {
                IWebElement topicElement;
                bool check = true;
                if (mail.Topic == null)
                {
                    topicElement = element.FindElement(By.ClassName("b-datalist__item__subj"));
                    TimeSpan dateTimeOfMail = TimeSpan.Parse(element.FindElement(By.ClassName("b-datalist__item__date__value")).Text);
                    if (Regex.IsMatch(topicElement.Text, this.pattern) && dateTimeOfMail.Hours == mail.Timestamp.Hours && dateTimeOfMail.Minutes == mail.Timestamp.Minutes)
                    {
                    }
                    else
                    {
                        check = false;
                    }
                }
                else
                {
                    var topic = string.Format("//*[contains(text(), {0})]", mail.Topic);
                    By topicBy = By.XPath(topic);
                    topicElement = element.FindElement(topicBy);
                }

                if (topicElement.Displayed && check)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsSearchMail(Mail mail)
        {
            try
            {
                var element = this.driver.FindElement(this.mailbox, 1);
                if (element.Displayed)
                {
                    var topic = string.Format("//*[contains(text(), {0})]", mail.Topic);
                    By topicBy = By.XPath(topic);
                    var topicElement = element.FindElement(topicBy);
                    if (topicElement.Displayed)
                    {
                        var checkbox = element.FindElement(this.checkboxMail);
                        checkbox.Click();
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }

            return false;
        }
    }
}
