﻿// <copyright file="Account.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Models
{
    public class Account
    {
        private string login;
        private string password;

        public Account(string login, string password)
        {
            this.login = login;
            this.password = password;
        }

        public string Login
        {
            get
            {
                return this.login;
            }

            set
            {
                value = this.login;
            }
        }

        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                value = this.password;
            }
        }
    }
}
