﻿// <copyright file="Mail.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Models
{
    public class Mail
    {
        private string to;
        private string topic;
        private string textOfMail;
        private string pathToFile;
        private TimeSpan timestamp;

        public Mail(string to, string topic, string textOfMail, string pathToFile)
        {
            this.to = to;
            this.topic = topic;
            this.textOfMail = textOfMail;
            this.pathToFile = pathToFile;
        }

        public Mail(string topic, string textOfMail)
            : this(null, topic, textOfMail, null)
        {
        }

        public Mail(string to)
            : this(to, null, null, null)
        {
        }

        public string To
        {
            get { return this.to; }
            set { this.to = value; }
        }

        public string Topic
        {
            get { return this.topic; }
            set { this.topic = value; }
        }

        public string PathToFile
        {
            get { return this.pathToFile; }
            set { this.pathToFile = value; }
        }

        public string TextOfMail
        {
            get { return this.textOfMail; }
            set { this.textOfMail = value; }
        }

        public TimeSpan Timestamp
        {
            get { return this.timestamp; }
            set { this.timestamp = value; }
        }
    }
}
