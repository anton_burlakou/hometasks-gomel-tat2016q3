﻿// <copyright file="MainScreen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrameworkLayer.Browser;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace FrameworkLayer.Screen
{
    public class MainScreen : IScreen
    {
        private const string CategoryName = "Часы";

        private readonly string baseUrl;

        private BrowserInstance browser;

        private By search = By.Id(@"search-cate");

        private By categorySearch = By.Id(@"search-dropdown-box");

        private By bannerBy = By.XPath(@"//div[contains(@class, 'ui-window-content')]");

        private By closeBy = By.XPath(@"//a[@data-role='layer-close']");

        #pragma warning restore 0649
        public MainScreen()
        {
           this.baseUrl = System.Configuration.ConfigurationManager.AppSettings["StartPage"];
           this.browser = new BrowserInstance();
        }

        public void OpenPage()
        {
            this.browser.OpenPage(this.baseUrl);
        }

        public void CloseBanner()
        {
           this.browser.FindElementIntoCtxAndClick(this.bannerBy, this.closeBy, false);
        }

        public string ClickCategory(string categoryText)
        {
            this.browser.Click(this.search);
            this.browser.FindElementIntoCtxAndClick(this.categorySearch, By.XPath(string.Format("//option[contains(text(), '{0}')]", CategoryName)), true);
            this.browser.FindElementIntoCtxAndClick(this.search, By.XPath("../input"), true, true);

            return CategoryName;
        }
    }
}
