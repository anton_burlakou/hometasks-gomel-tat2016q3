﻿// <copyright file="SearchScreen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Browser;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FrameworkLayer.Screen.Aliexpress
{
    public class SearchScreen : IScreen
    {
        private BrowserInstance browser;

        private By brandBy = By.XPath(@"//div[@id='brands-wall-box']//img[@alt='Skmei']");

        private By crumbBy = By.XPath(@"//div[@id='aliGlobalCrumb']");

        private By productBy = By.XPath(@"//div[@id='list-items']//li[contains(@class, 'list-item-first')]");

        public SearchScreen()
        {
            this.browser = new BrowserInstance();
        }

        public void ClickBrandName()
        {
            this.browser.Click(this.brandBy);
        }

        public bool IsBrandSelected()
        {
            if (this.browser.FindElement(this.brandBy).Displayed)
            {
                var classesBrand = this.browser.FindElementIntoCtx(this.brandBy, By.XPath(@"../.."), false).GetAttribute("class");

                if (Regex.IsMatch(classesBrand, "selected"))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsCategoryExist(string categoryName)
        {
            var title = this.browser.FindElementIntoCtx(this.crumbBy, By.XPath(string.Format("//span[contains(text(), {0})]", categoryName)), false);
            if (title.Displayed)
            {
                return true;
            }

            return false;
        }

        public string ClickFirstProductAfterSearch()
        {
            var productDescription = this.browser.FindElementIntoCtx(this.productBy, By.XPath("//a[contains(@class, 'product')]"), false);
            var title = productDescription.GetAttribute("title");
            this.browser.Click(productDescription);

            return title;
        }
    }
}
