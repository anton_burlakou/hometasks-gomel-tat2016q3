﻿// <copyright file="BasketScreen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Browser;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FrameworkLayer.Screen.Aliexpress
{
    public class BasketScreen
    {
        private BrowserInstance browser;

        private By productTitleBy = By.XPath(@"//table[contains(@class, 'item-group')]//a[contains(@class, 'lnk-product-name')]");

        private By removeAllBy = By.ClassName("remove-all-product");

        private By confirmBy = By.XPath(@"//div[contains(@class, 'ui-window-btn')]//input[@data-role='submit']");

        private By notificationBy = By.Id("notice");

        public BasketScreen()
        {
            this.browser = new BrowserInstance();
        }

        public bool IsProductExistIntoBasket(string productName)
        {
            var product = this.browser.FindElement(this.productTitleBy);
            if (Regex.IsMatch(product.Text, productName))
            {
                return true;
            }

            return false;
        }

        public void RemoveItemFromBasket()
        {
            this.browser.Click(this.removeAllBy);
            this.browser.Click(this.confirmBy);
        }

        public bool IsProductRemoveIntoBasket()
        {
           var notification = this.browser.FindElement(this.notificationBy);
            if (Regex.IsMatch(notification.Text, "Ваша Корзина пуста"))
            {
                return true;
            }

            return false;
        }
    }
}
