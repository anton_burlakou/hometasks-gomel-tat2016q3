﻿// <copyright file="LeftMenuPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Browser;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Screen.MailRu
{
    public class LeftMenuPage : IScreen
    {
        private BrowserInstance browser;

        private By writeMail = By.XPath("//div[contains(@class, 'b-sticky') and not(contains(@class, 'js-not-sticky'))]//a[@data-bem='b-toolbar__btn']");

        private By sentBox = By.XPath("//a[@href='/messages/sent/']");

        private By inbox = By.XPath("//a[@href='/messages/inbox/']");

        private By draft = By.XPath("//a[@href='/messages/drafts/']");

        private By trash = By.XPath("//a[@href='/messages/trash/']");

        public LeftMenuPage()
        {
            this.browser = new BrowserInstance();
        }

        public void ClickButtonWriteMail()
        {
            this.Click(this.writeMail);
        }

        public void ClickLinkSendbox()
        {
            this.Click(this.sentBox);
        }

        public void ClickLinkInbox()
        {
            this.Click(this.inbox);
        }

        public void ClickLinkDraft()
        {
            this.Click(this.draft);
        }

        public void ClickLinkTrash()
        {
            this.Click(this.trash);
        }

        private void Click(By el)
        {
            this.browser.Click(el);
        }
    }
}
