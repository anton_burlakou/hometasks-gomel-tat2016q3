﻿// <copyright file="TopBarPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Browser;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Screen.MailRu
{
    public class TopBarPage : IScreen
    {
        private BrowserInstance browser;

        private By greeting = By.Id("PH_user-email");

        public TopBarPage()
        {
            this.browser = new BrowserInstance();
        }

        public bool IsLogged()
        {
            var element = this.browser.FindElement(this.greeting);
            if (element != null && element.Displayed)
            {
                return true;
            }

            return false;
        }
    }
}
