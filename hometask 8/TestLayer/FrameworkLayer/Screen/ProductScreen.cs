﻿// <copyright file="ProductScreen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Browser;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FrameworkLayer.Screen
{
    public class ProductScreen
    {
        private BrowserInstance browser;

        private By productNameBy = By.ClassName("product-name");

        private By colorBy = By.XPath(@"//div[@id='j-product-info-sku']//li");

        private By addToBasket = By.Id("j-add-cart-btn");

        private By redirectToBasket = By.XPath("//div[contains(@class, 'ui-window-btn')]/a");

        public ProductScreen()
        {
            this.browser = new BrowserInstance();
        }

        public bool IsProductExist(string productDescription)
        {
            var productTitle = this.browser.FindElement(this.productNameBy);
            if (Regex.IsMatch(productTitle.Text, productDescription))
            {
                return true;
            }

            return false;
        }

        public void ClickAddToBasket()
        {
            this.browser.Click(this.colorBy);
            this.browser.Click(this.addToBasket);
            this.browser.Click(this.redirectToBasket);
        }
    }
}
