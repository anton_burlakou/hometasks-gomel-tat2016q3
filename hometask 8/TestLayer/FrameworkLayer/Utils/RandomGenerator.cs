﻿// <copyright file="RandomGenerator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Utils
{
    public class RandomGenerator
    {
        public static string GetRandomString(int length)
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random = new Random();
            string result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }

        public static string GetRandomFile()
        {
            Random rnd = new Random();
            var path = Path.GetTempFileName();
            using (FileStream fs = File.Open(path, FileMode.Open, FileAccess.Write, FileShare.None))
            {
                byte[] info = new UTF8Encoding(true).GetBytes(GetRandomString(rnd.Next(1, 40)));
                fs.Write(info, 0, info.Length);
            }

            return path;
        }

        public static List<string> GetRandomFiles(int count)
        {
            return Enumerable.Repeat(string.Empty, count).Select(e => e = GetRandomFile()).ToList();
        }

        public static string GetMD5(string path)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(path))
                {
                    return Encoding.Default.GetString(md5.ComputeHash(stream));
                }
            }
        }
    }
}
