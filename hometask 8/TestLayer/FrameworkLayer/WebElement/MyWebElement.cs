﻿// <copyright file="MyWebElement.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.WebElement
{
    public class MyWebElement : IMyWebElement
    {
        private IWebElement webElement;

        private IWebDriver driver;

        public MyWebElement()
        {
            this.driver = Browser.DriverInstance.GetInstance();
        }

        public bool Displayed
        {
            get
            {
                return this.webElement.Displayed;
            }
        }

        public bool Enabled
        {
            get
            {
                return this.webElement.Enabled;
            }
        }

        public Point Location
        {
            get
            {
                return this.webElement.Location;
            }
        }

        public bool Selected
        {
            get
            {
                return this.webElement.Selected;
            }
        }

        public Size Size
        {
            get
            {
                return this.webElement.Size;
            }
        }

        public string TagName
        {
            get
            {
                return this.webElement.TagName;
            }
        }

        public string Text
        {
            get
            {
                return this.webElement.Text;
            }
        }

        public void Clear()
        {
            this.webElement.Click();
        }

        public void Click()
        {
            this.webElement.Click();
        }

        public void DoubleClick(IWebElement source)
        {
            Actions action = new Actions(this.driver);
            action.DoubleClick(source);
            action.Build().Perform();
        }

        public void DragAndDrop(IWebElement source, IWebElement target)
        {
            Actions action = new Actions(this.driver);
            action.DragAndDrop(source, target);
            action.Build().Perform();
        }

        public void CtrlClickElements(IWebElement ctx, IWebElement target, List<string> listEls)
        {
            Actions action = new Actions(this.driver);
            action.KeyDown(Keys.LeftControl);
            foreach (var file in listEls)
            {
                var element = ctx.FindElementIntoElementWithTimeOut(By.XPath(file));
                action.Click(element);
            }

            action.KeyUp(Keys.LeftControl)
                .ClickAndHold()
                .MoveToElement(target)
                .Release()
                .Build().Perform();
        }

        public IWebElement FindElement(By by)
        {
            return this.webElement.FindElement(by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return this.webElement.FindElements(by);
        }

        public string GetAttribute(string attributeName)
        {
            return this.webElement.GetAttribute(attributeName);
        }

        public string GetCssValue(string propertyName)
        {
            return this.webElement.GetCssValue(propertyName);
        }

        public void SendKeys(string text)
        {
            this.webElement.SendKeys(text);
        }

        public void Submit()
        {
            this.webElement.Submit();
        }
    }
}
