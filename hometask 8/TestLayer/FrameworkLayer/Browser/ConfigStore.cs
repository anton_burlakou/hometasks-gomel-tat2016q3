﻿// <copyright file="ConfigStore.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Browser
{
    public static class ConfigStore
    {
        private static Configuration store;

        public static void SetConfigure(Configuration config)
        {
            store = config;
        }

        public static Configuration GetConfigure()
        {
            return store;
        }
    }
}
