﻿// <copyright file="BrowserType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Browser
{
    public enum BrowserType
    {
        Chrome,
        Firefox
    }
}
