﻿// <copyright file="Configuration.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Browser
{
    public class Configuration
    {
        private string host;
        private string port;
        private string browser;
        private string fullhost;

        public Configuration()
        {
        }

        public Configuration(string host, string port, string browser)
        {
            this.host = host;
            this.port = port;
            this.browser = browser;
        }

        public string Host
        {
            get
            {
                return this.host;
            }

            set
            {
                this.host = value;
            }
        }

        public string FullHost
        {
            get
            {
                return this.fullhost;
            }

            set
            {
                this.fullhost = value;
            }
        }

        public string Port
        {
            get
            {
                return this.port;
            }
        }

        public string Browser
        {
            get
            {
                return this.browser;
            }
        }

        public static string GetProjectConfig()
        {
            var browserType = ConfigStore.GetConfigure()?.Browser;
            if (browserType != null)
            {
                return browserType;
            }

            return System.Configuration.ConfigurationManager.AppSettings["Browser"];
        }

        public static BrowserType GetTypeFromConfig()
        {
            switch (GetProjectConfig())
            {
                case "Firefox":
                    return BrowserType.Firefox;
                default:
                    return BrowserType.Chrome;
            }
        }
    }
}
