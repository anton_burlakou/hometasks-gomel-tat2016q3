﻿// <copyright file="AuthorizationService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Models;
using FrameworkLayer.Screen.MailRu;
using FrameworkLayer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Service
{
    public class AuthorizationService
    {
        private Account validAccount;

        private Account incorrectAccount;

        private LoginPage loginPage = new LoginPage();

        public AuthorizationService()
        {
            var login = System.Configuration.ConfigurationManager.AppSettings["LoginMail"];
            var password = System.Configuration.ConfigurationManager.AppSettings["Password"];
            this.validAccount = new Account(login, password);
            var random = new System.Random();
            this.incorrectAccount = new Account(RandomGenerator.GetRandomString(random.Next(0, 10)), RandomGenerator.GetRandomString(random.Next(0, 10)));
        }

        public void CorrectLogin()
        {
            this.loginPage.Login(this.validAccount);
        }

        public void IncorrectLogin()
        {
            this.loginPage.Login(this.incorrectAccount);
        }

        public void OpenPage()
        {
            this.loginPage.OpenPage();
        }

        public bool IsLoggedIn()
        {
            var topbar = new TopBarPage();
            return topbar.IsLogged();
        }
    }
}
