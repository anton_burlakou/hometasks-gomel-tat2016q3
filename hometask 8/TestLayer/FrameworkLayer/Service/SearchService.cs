﻿// <copyright file="SearchService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrameworkLayer.Screen.Aliexpress;

namespace FrameworkLayer.Service
{
    public class SearchService
    {
        private MainScreen mainPage;
        private SearchScreen searchPage;
        private ProductScreen productPage;
        private BasketScreen basketPage;
        private string textCategory;
        private string productDescription;

        public SearchService()
        {
            this.mainPage = new MainScreen();
            this.searchPage = new SearchScreen();
            this.productPage = new ProductScreen();
            this.basketPage = new BasketScreen();
        }

        public void OpenPage()
        {
            this.mainPage.OpenPage();
            this.mainPage.CloseBanner();
        }

        public void ClickCategoryOnPage()
        {
            this.textCategory = this.mainPage.ClickCategory(this.textCategory);
        }

        public void ClickBrandOnPage()
        {
            this.searchPage.ClickBrandName();
        }

        public bool CategoryIsExist()
        {
            return this.searchPage.IsCategoryExist(this.textCategory);
        }

        public void ClickOnFirstProduct()
        {
            this.productDescription = this.searchPage.ClickFirstProductAfterSearch();
        }

        public bool IsProductExist()
        {
            return this.productPage.IsProductExist(this.productDescription);
        }

        public bool IsBrandSelected()
        {
            return this.searchPage.IsBrandSelected();
        }

        public void ClickToBasket()
        {
            this.productPage.ClickAddToBasket();
        }

        public bool IsProductExistIntoBasket()
        {
            return this.basketPage.IsProductExistIntoBasket(this.productDescription);
        }

        public void RemoveProductFromBasket()
        {
            this.basketPage.RemoveItemFromBasket();
        }

        public bool IsProductRemoveIntoBasket()
        {
            return this.basketPage.IsProductRemoveIntoBasket();
        }
    }
}
