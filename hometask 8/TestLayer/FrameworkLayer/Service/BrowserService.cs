﻿// <copyright file="BrowserService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrameworkLayer.Browser;

namespace FrameworkLayer.Service
{
    public class BrowserService
    {
        public void Open()
        {
            DriverInstance.GetInstance();
        }

        public void Quit()
        {
            DriverInstance.CloseBrowser();
        }
    }
}
