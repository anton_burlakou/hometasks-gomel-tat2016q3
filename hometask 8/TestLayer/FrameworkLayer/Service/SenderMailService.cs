﻿// <copyright file="SenderMailService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Models;
using FrameworkLayer.Screen.MailRu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.Service
{
    public class SenderMailService
    {
        private Mail mail;

        public SenderMailService()
        {
            this.mail = MailFactory.CreateMail(MailParams.FullWithCorrectMail);
        }

        public SenderMailService(MailParams config)
        {
            this.mail = MailFactory.CreateMail(config);
        }

        public void ClickButtonWriteMail()
        {
            var leftMenuPage = new LeftMenuPage();
            leftMenuPage.ClickButtonWriteMail();
        }

        public void CreateMailAsTemplate()
        {
            MainContextMailPage mainContextMailPage = new MainContextMailPage();
            mainContextMailPage.FillTheMailAndSaveAsTemplate(this.mail);
            LeftMenuPage leftMenu = new LeftMenuPage();
            leftMenu.ClickLinkDraft();
            MailContextPage ctx = new MailContextPage();
            ctx.IsSearchMail(this.mail);
        }

        public void RemoveTemplate()
        {
            MainContextMailPage mainContextMailPage = new MainContextMailPage();
            mainContextMailPage.RemoveMail();
        }

        public void FillTheMail()
        {
            var mainContextMailPage = new MainContextMailPage();
            mainContextMailPage.SendMail(this.mail);
            this.mail.Timestamp = DateTime.Now.TimeOfDay;
        }

        public bool IsAlertExist()
        {
            var mail = new MainContextMailPage();
            return mail.IsAlertExist();
        }

        public bool IsMailDeleted()
        {
            var leftMenuPage = new LeftMenuPage();
            leftMenuPage.ClickLinkTrash();
            MailContextPage ctx = new MailContextPage();
            if (ctx.IsSearchMail(this.mail))
            {
                MainContextMailPage mainContextMailPage = new MainContextMailPage();
                mainContextMailPage.RemoveMail();
                if (!ctx.IsSearchMail(this.mail))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsMailExist()
        {
            var leftMenuPage = new LeftMenuPage();
            leftMenuPage.ClickLinkSendbox();

            var mailpage = new MailContextPage();
            if (mailpage.CheckMail(this.mail))
            {
                leftMenuPage.ClickLinkInbox();
                return mailpage.CheckMail(this.mail);
            }

            return false;
        }
    }
}
