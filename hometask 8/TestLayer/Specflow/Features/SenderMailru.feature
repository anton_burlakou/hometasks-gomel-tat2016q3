﻿Feature: Checking sender mail service
In order to send mail
As authorize user
I want to send mail with different possibility

@OpenPage
Scenario: Send mail with full data
	Given: I open the mail.ru page
	When: I clicked button sing in
	And I fill all fields
	Then The mail should be sent

@OpenPage
Scenario: Send mail without mail adress
	Given: I open the mail.ru page
	When: I clicked button sing in
	And I fill fields without mail adress
	Then The result should be alert

@OpenPage
Scenario: Send mail without topic and body
	Given: I open the mail.ru page
	When: I clicked button sing in
	And I fill fields without topic and body
	Then The mail should be sent