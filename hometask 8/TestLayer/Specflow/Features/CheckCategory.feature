﻿Feature: Checking a main function with product 
In order to realize new feature
As anonymous user
I want checking category into navigation
And checking brand into category
And checking product into brand
And also I need to add product to basket
And remove it from basket

@OpenPage
Scenario: Check category from menu
	Given I open the aliexpress page
	When I clicked the category of whatches on page
	Then The result should contain text with category name

@OpenPage
Scenario: Check extended search by brand name
	Given I open the aliexpress page
	When I clicked the category of whatches on page
	And  I clicked brand by 'Skmei name
	Then The brand name should be selected

@OpenPage
Scenario: Check product into subcategory
	Given I open the aliexpress page
	When I clicked the category of whatches on page
	And I clicked brand by 'Skmei name
	And I clicked on first product
	Then The result should contain text with product name

@OpenPage
Scenario: Add product to basket
	Given I open the aliexpress page
	When I clicked the category of whatches on page
	And I clicked brand by 'Skmei name
	And I clicked on first product
	And I clicked to add to basket
	Then The basket should contain this product

@OpenPage
Scenario: Remove product from basket
	Given I open the aliexpress page
	When I clicked the category of whatches on page
	And I clicked brand by 'Skmei name
	And I clicked on first product
	And I clicked to add to basket
	And I clicked remove product from basket
	Then The basket not should contain this product


