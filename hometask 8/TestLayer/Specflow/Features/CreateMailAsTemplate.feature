﻿Feature: Create mail
In order to create as template
As authorize user
I want to create mail as template
And remove it

@OpenPage
Scenario: Create mail as template and remove it
	Given: I open the mail.ru page
	When: I clicked button sing in
	And I clicked button save as template
	And Remove this template
	Then The mail should be removed

