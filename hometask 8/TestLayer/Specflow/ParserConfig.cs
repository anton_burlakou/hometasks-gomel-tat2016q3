﻿// <copyright file="ParserConfig.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Specflow
{
    public static class ParserConfig
    {
        private static Dictionary<string, string> config;

        static ParserConfig()
        {
            config = new Dictionary<string, string>();
        }

        public static Dictionary<string, string> Parse(string[] args)
        {
            foreach (var arg in args)
            {
                var keyValue = arg.Split('=');
                if (keyValue.Length > 1)
                {
                    config.Add(keyValue[0], keyValue[1]);
                }
            }

            return config;
        }
    }
}
