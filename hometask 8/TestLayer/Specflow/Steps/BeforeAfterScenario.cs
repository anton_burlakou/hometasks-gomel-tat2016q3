﻿// <copyright file="BeforeAfterScenario.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using FrameworkLayer.Service;
using FrameworkLayer.Models;
using NUnit.Framework.Interfaces;

namespace Specflow.Steps
{
    [Timeout(30000)]
    [Binding]
    public class BeforeAfterScenario
    {
        private static BrowserService browser = new BrowserService();

        [BeforeScenario("OpenPage")]
        public static void BeforeScenario()
        {
            BeforeAfterTest.Report.StartTest(TestContext.CurrentContext.Test.Name);
            browser.Open();
        }

        [AfterScenario("OpenPage")]
        public static void AfterScenario()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stackTrace = string.Format("<pre>{0}</pre>", TestContext.CurrentContext.Result.StackTrace);
            var errorMessage = TestContext.CurrentContext.Result.Message;

            Error logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Error.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Error.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Error.Skip;
                    break;
                default:
                    logstatus = Error.Pass;
                    break;
            }

            BeforeAfterTest.Report.GetReportResult(logstatus, stackTrace + errorMessage);
            browser.Quit();
        }
    }
}
