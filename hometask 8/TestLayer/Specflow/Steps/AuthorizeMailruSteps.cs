﻿// <copyright file="AuthorizeMailruSteps.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using FrameworkLayer.Service;
using NUnit;
using NUnit.Framework;

namespace Specflow.Steps
{
    [Binding]
    public class AuthorizeMailruSteps
    {
        private AuthorizationService authorize;

        public AuthorizeMailruSteps()
        {
            this.authorize = new AuthorizationService();
        }

        [Given(@"I open the mailru page")]
        public void OpenPage()
        {
            this.authorize.OpenPage();
        }

        [When(@"I clicked button sing in")]
        public void SignIn()
        {
            this.authorize.CorrectLogin();
        }

        [When(@"I clicked button sing in with incorrect data")]
        public void SignInWithIncorectData()
        {
            this.authorize.IncorrectLogin();
        }

        [Then(@"The result should contain greeting user with correct data")]
        public void AuthorizeSuccess()
        {
            Assert.True(this.authorize.IsLoggedIn());
        }

        [Then(@"The result should contain user not found")]
        public void AuthorizeUnsuccessfull()
        {
            Assert.False(this.authorize.IsLoggedIn());
        }
    }
}
