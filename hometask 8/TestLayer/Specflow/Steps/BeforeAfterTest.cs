﻿// <copyright file="BeforeAfterTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using NUnit.Framework;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using FrameworkLayer.Service;

namespace Specflow.Steps
{
    [Timeout(3000)]
    [Binding]
    public class BeforeAfterTest
    {
        public static ReportService Report = new ReportService();

        [BeforeTestRun]
        public static void BeforeTest()
        {
            var parameters = TestContext.Parameters;
            var dictParams = new Dictionary<string, string>();
            if (parameters.Count > 0 && ConfigStore.GetConfigure() == null)
            {
                foreach (var param in parameters.Names)
                {
                    dictParams.Add(param, parameters.Get(param));
                    System.Console.WriteLine("key - {0}, value - {1}", param, parameters.Get(param));
                }

                var configBuilder = new ConfigureBuilder();
                ConfigStore.SetConfigure(configBuilder.CreateConfigureTest(dictParams));
                FrameworkLayer.Browser.ConfigStore.SetConfigure(ConfigStore.GetConfigureDTO());
            }

            Report.Start();
        }

        [AfterTestRun]
        public static void AfterTest()
        {
            Report.Finish();
        }
    }
}
