﻿// <copyright file="SenderMailruSteps.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrameworkLayer.Service;
using TechTalk.SpecFlow;
using NUnit.Framework;
using FrameworkLayer.Models;

namespace Specflow.Steps
{
    [Binding]
    public class SenderMailruSteps
    {
        private AuthorizationService authorize;
        private SenderMailService sender;

        public SenderMailruSteps()
        {
            this.authorize = new AuthorizationService();
            this.sender = new SenderMailService();
        }

        [Given(@"I open the mailru page")]
        public void OpenPage()
        {
            this.authorize.OpenPage();
        }

        [When(@"I clicked button sing in")]
        public void SignIn()
        {
            this.authorize.CorrectLogin();
        }

        [When(@"I fill all fields")]
        public void FillAllFields()
        {
            this.sender.ClickButtonWriteMail();
            this.sender.FillTheMail();
        }

        [When(@"I fill fields without mail adress")]
        public void FillWithoutMailAdress()
        {
            this.sender = new SenderMailService(MailParams.WithoutAdress);
            this.sender.ClickButtonWriteMail();
            this.sender.FillTheMail();
        }

        [When(@"I fill fields without topic and body")]
        public void FillWithoutTopicAndBody()
        {
            this.sender = new SenderMailService(MailParams.WithoutSubjectAndBody);
            this.sender.ClickButtonWriteMail();
            this.sender.FillTheMail();
        }

        [Then(@"The mail should be sent")]
        public void MailShouldBeExist()
        {
            Assert.True(this.sender.IsMailExist());
        }

        [Then(@"The result should be alert")]
        public void AlertShouldBeExist()
        {
            Assert.True(this.sender.IsAlertExist());
        }
    }
}
