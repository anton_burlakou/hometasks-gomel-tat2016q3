﻿// <copyright file="CreateMailAsTemplateSteps.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Service;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Specflow.Steps
{
    [Binding]
    public class CreateMailAsTemplateSteps
    {
            private AuthorizationService authorize;
            private SenderMailService sender;

            public CreateMailAsTemplateSteps()
            {
                this.authorize = new AuthorizationService();
                this.sender = new SenderMailService();
            }

            [Given(@"I open the mailru page")]
            public void OpenPage()
            {
                this.authorize.OpenPage();
            }

            [When(@"I clicked button sing in")]
            public void SignIn()
            {
                this.authorize.CorrectLogin();
            }

            [When(@"I clicked button save as template")]
            public void CreateAsTemplate()
            {
                this.sender.ClickButtonWriteMail();
                this.sender.CreateMailAsTemplate();
            }

            [When(@"Remove this template")]
            public void RemoveMail()
            {
                this.sender.RemoveTemplate();
            }

            [Then(@"The mail should be removed")]
            public void MailIsRemoved()
            {
                Assert.True(this.sender.IsMailDeleted());
            }
    }
}
