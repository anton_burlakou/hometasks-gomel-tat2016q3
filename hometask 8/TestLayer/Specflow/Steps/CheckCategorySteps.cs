﻿// <copyright file="CheckCategorySteps.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TechTalk.SpecFlow;
using FrameworkLayer.Service;

namespace Specflow.Steps
{
    [Binding]
    public class CheckCategorySteps
    {
        private SearchService search;

        public CheckCategorySteps()
        {
            this.search = new SearchService();
        }

        [Given(@"I open the aliexpress page")]
        public void OpenPage()
        {
            this.search.OpenPage();
        }

        [When(@"I clicked the category of whatches on page")]
        public void ClickCategory()
        {
            this.search.ClickCategoryOnPage();
        }

        [When(@"I clicked brand by 'Skmei name")]
        public void ClickSubCategory()
        {
            this.search.ClickBrandOnPage();
        }

        [When(@"I clicked on first product")]
        public void ClickFirstProductAfterSearch()
        {
            this.search.ClickOnFirstProduct();
        }

        [When(@"I clicked to add to basket")]
        public void ClickToBasket()
        {
            this.search.ClickToBasket();
        }

        [When(@"I clicked remove product from basket")]
        public void RemoveProductFromBasket()
        {
            this.search.RemoveProductFromBasket();
        }

        [Then(@"The result should contain text with category name")]
        public void CategoryMustBe()
        {
            Assert.True(this.search.CategoryIsExist());
        }

        [Then(@"The brand name should be selected")]
        public void CategoryMustBeSelected()
        {
            Assert.True(this.search.IsBrandSelected());
        }

        [Then(@"The result should contain text with product name")]
        public void ProductMustbe()
        {
            Assert.True(this.search.IsProductExist());
        }

        [Then(@"The basket should contain this product")]
        public void BasketMustContainProduct()
        {
            Assert.True(this.search.IsProductExistIntoBasket());
        }

        [Then(@"The basket not should contain this product")]
        public void BasketNotShouldContainProduct()
        {
            Assert.True(this.search.IsProductRemoveIntoBasket());
        }
    }
}
