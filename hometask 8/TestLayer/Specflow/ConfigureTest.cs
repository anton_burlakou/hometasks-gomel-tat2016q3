﻿// <copyright file="ConfigureTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Specflow
{
    public class ConfigureTest
    {
        private string host;
        private string port;
        private string browser;
        private string thread;

        public ConfigureTest()
        {
        }

        public ConfigureTest(string host, string port, string browser, string thread)
        {
            this.host = host;
            this.port = port;
            this.browser = browser;
            this.thread = thread;
        }

        public string Host
        {
            get
            {
                return this.host;
            }

            set
            {
                this.host = value;
            }
        }

        public string Port
        {
            get
            {
                return this.port;
            }

            set
            {
                this.port = value;
            }
        }

        public string Browser
        {
            get
            {
                return this.browser;
            }

            set
            {
                this.browser = value;
            }
        }

        public string Thread
        {
            get
            {
                return this.thread;
            }

            set
            {
                this.thread = value;
            }
        }
    }
}
