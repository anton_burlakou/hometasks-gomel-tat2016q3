﻿//-----------------------------------------------------------------------
// <copyright file="MailFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Utils;
using System;
using System.IO;
using System.Text;

namespace FrameworkLayer.Models
{
    public enum MailParams
    {
        WithoutSubjectAndBody = 1,
        WithoutAdress,
        Full,
        FullWithCorrectMail
    }

    public static class MailFactory
    {
        public static Mail CreateMail(MailParams enumParams)
        {
            Random rnd = new Random();
            var to = System.Configuration.ConfigurationManager.AppSettings["MailForSend"];
            var path = string.Empty;
            if ((int)enumParams >= 3)
            {
                path = RandomGenerator.GetRandomFile();
            }

            switch (enumParams)
            {
                case MailParams.WithoutAdress:
                    return new Mail(RandomGenerator.GetRandomString(rnd.Next(1, 40)), RandomGenerator.GetRandomString(rnd.Next(1, 40)));
                case MailParams.WithoutSubjectAndBody:
                    return new Mail(to);
                case MailParams.Full:
                    return new Mail(RandomGenerator.GetRandomString(rnd.Next(1, 40)), RandomGenerator.GetRandomString(rnd.Next(1, 40)), RandomGenerator.GetRandomString(rnd.Next(1, 40)), path);
                default:
                    return new Mail(to, RandomGenerator.GetRandomString(rnd.Next(1, 40)), RandomGenerator.GetRandomString(rnd.Next(1, 40)), path);
            }
        }
    }
}
