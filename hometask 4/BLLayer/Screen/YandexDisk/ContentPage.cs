﻿// <copyright file="ContentPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using FrameworkLayer.Browser;
using OpenQA.Selenium.Interactions;

namespace FrameworkLayer.POM.YandexDisk
{
    public class ContentPage
    {
        private IWebDriver driver;

        private By content = By.XPath("//div[contains(@class, 'b-content')]//div[contains(@class, 'b-listing__chunk')]");

        private By dlButton = By.XPath("//button[@data-click-action='resource.download']");

        private By trashBy = By.XPath("//div[contains(@class, 'b-content')]//div[contains(@class, 'b-listing__chunk')]//div[contains(text(), 'Корзина')]");

        public ContentPage()
        {
            this.driver = DriverInstance.GetInstance();
        }

        public string GetCurrentUrl()
        {
            return this.driver.Url;
        }

        public void Download(string path)
        {
            var ctx = this.driver.FindElementWithTimeOutDefault(this.content, true);
            var element = ctx.FindElementIntoElementWithTimeOut(By.XPath(string.Format("//div[contains(text(), '{0}')]", System.IO.Path.GetFileName(path))));
            element.Click();
            this.driver.FindElementWithTimeOutDefault(this.dlButton).Click();
        }

        public void MovedToTrash(string path)
        {
            var ctx = this.driver.FindElementWithTimeOutDefault(this.content, true);
            var element = ctx.FindElementIntoElementWithTimeOut(By.XPath(string.Format("//div[contains(text(), '{0}')]", System.IO.Path.GetFileName(path))));
            var trash = this.driver.FindElementWithTimeOutDefault(this.trashBy);
            Actions action = new Actions(this.driver);
            action.DragAndDrop(element, trash);
            action.Build().Perform();
        }

        public void MovedToTrashFilesWithCtrlClick(List<string> filepathList)
        {
            Actions action = new Actions(this.driver);
            var ctx = this.driver.FindElementWithTimeOutDefault(this.content, true);
            var trash = this.driver.FindElementIntoElementWithTimeOut(this.trashBy);
            action.KeyDown(Keys.LeftControl);
            foreach (var file in filepathList)
            {
                var element = ctx.FindElementIntoElementWithTimeOut(By.XPath(string.Format("//div[contains(text(), '{0}')]", System.IO.Path.GetFileName(file))));
                action.Click(element);
            }

            action.KeyUp(Keys.LeftControl)
                .ClickAndHold()
                .MoveToElement(trash)
                .Release()
                .Build().Perform();
        }

        public void OpenTrash()
        {
            var trash = this.driver.FindElementWithTimeOutDefault(this.trashBy);
            Actions action = new Actions(this.driver);
            action.DoubleClick(trash);
            action.Build().Perform();
        }

        public bool IsRestore(string path)
        {
            try
            {
                var ctx = this.driver.FindElementWithTimeOutDefault(this.content, true);
                var element = ctx.FindElementIntoElementWithTimeOut(By.XPath(string.Format("//div[contains(text(), '{0}')]", System.IO.Path.GetFileName(path))));
                if (element.Displayed)
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }

            return false;
        }
    }
}
