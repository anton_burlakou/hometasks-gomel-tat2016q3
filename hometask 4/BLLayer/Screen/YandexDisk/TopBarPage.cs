﻿// <copyright file="TopBarPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using FrameworkLayer.Browser;
using FrameworkLayer.Utils;

namespace FrameworkLayer.POM.YandexDisk
{
    public class TopBarPage
    {
        private IWebDriver driver;

        private By attachFile = By.XPath("//input[contains(@class, 'button__attach')]");

        public TopBarPage()
        {
            this.driver = DriverInstance.GetInstance();
        }

        public void Upload(string path)
        {
            this.driver.FindElementWithTimeOutDefault(this.attachFile, true).SendKeys(path);
        }
    }
}
