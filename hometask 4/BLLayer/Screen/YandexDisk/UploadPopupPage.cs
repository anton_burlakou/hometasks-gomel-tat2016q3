﻿// <copyright file="UploadPopupPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using FrameworkLayer.Browser;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkLayer.POM.YandexDisk
{
    public class UploadPopupPage
    {
        private IWebDriver driver;

        private By popupTitle = By.CssSelector("div._nb-modal-popup div._nb-popup-title div");

        private By close = By.CssSelector("div._nb-modal-popup a[data-click-action='dialog.close']");

        public UploadPopupPage()
        {
            this.driver = DriverInstance.GetInstance();
        }

        public bool IsUpload()
        {
            var title = this.driver.FindElementWithTimeOutDefault(this.popupTitle);
            if (title.Displayed)
            {
                if (Equals(title.Text, "Загрузка завершена"))
                {
                         return true;
                }

                return false;
            }

            return false;
        }

        public void Close()
        {
            this.driver.FindElementWithTimeOutDefault(this.close).Click();
        }
    }
}
