﻿// <copyright file="LoginPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using FrameworkLayer.POM;
using FrameworkLayer.Models;
using FrameworkLayer.Browser;
using OpenQA.Selenium.Support.PageObjects;

namespace FrameworkLayer.POM.YandexDisk
{
    public class LoginPage : ILoginPage
    {
        private readonly string baseUrl = System.Configuration.ConfigurationManager.AppSettings["StartPageYandex"];

        private IWebDriver driver;

        #pragma warning disable 0649
        [FindsBy(How = How.XPath, Using = "//input[@name='login']")]
        private IWebElement inputLogin;

        [FindsBy(How = How.XPath, Using = "//input[@name='passwd']")]
        private IWebElement inputPassword;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class, 'button auth__button domik2__auth-button')]")]
        private IWebElement buttonSubmit;
        #pragma warning restore 0649

        public LoginPage()
        {
            this.driver = DriverInstance.GetInstance();
            PageFactory.InitElements(this.driver, this);
        }

        public void Login(Account account)
        {
            this.inputLogin.SendKeys(account.Login);
            this.inputPassword.SendKeys(account.Password);
            this.buttonSubmit.Click();
            ////Search button google disk after page load
            this.driver.FindElementIntoElementWithTimeOut(By.XPath("//a[@data-id='disk']")).Click();
        }

        public void OpenPage()
        {
            this.driver.Navigate().GoToUrl(this.baseUrl);
        }
    }
}
