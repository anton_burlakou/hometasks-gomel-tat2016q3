﻿// <copyright file="TrashPopupPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using OpenQA.Selenium;
using FrameworkLayer.Browser;

namespace FrameworkLayer.POM.YandexDisk
{
    public class TrashPopupPage
    {
        private IWebDriver driver;

        private By confirmButtonBy = By.XPath("//div[contains(@class, '_nb-popup b-confirmation ')]//button[contains(@class, 'js-confirmation-accept')]");

        public TrashPopupPage()
        {
            this.driver = DriverInstance.GetInstance();
        }

        public void Confirm()
        {
            var confirmButton = this.driver.FindElementIntoElementWithTimeOut(this.confirmButtonBy);
            if (confirmButton.Displayed)
            {
                confirmButton.Click();
            }
        }
    }
}
