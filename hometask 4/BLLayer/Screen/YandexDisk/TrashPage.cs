﻿// <copyright file="TrashPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using FrameworkLayer.Browser;

namespace FrameworkLayer.POM.YandexDisk
{
    public class TrashPage
    {
        private IWebDriver driver;

        private By restoreBy = By.XPath("//button[@data-click-action='resource.restore']");

        private By contentBy = By.XPath("//div[@data-key='box=boxListing']");

        private By removeAllBy = By.XPath("//div[contains(@class, 'toolset__buttons')]//button[contains(@class, 'button_js_inited')]");

        public TrashPage()
        {
            this.driver = DriverInstance.GetInstance();
        }

        public bool IsExist(string path)
        {
            try
            {
                var file = this.SearchElement(path);
                if (file.Displayed)
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }

            return false;
        }

        public bool IsExist(List<string> filepathList)
        {
            var count = filepathList.Count;
            for (var i = 0; i < count; i++)
            {
                var check = this.IsExist(filepathList[i]);
                if (!check)
                {
                    break;
                }

                if (i == count - 1 && check)
                {
                    return true;
                }
            }

            return false;
        }

        public void Restore(string path)
        {
            var file = this.SearchElement(path);
            if (file.Displayed)
            {
                file.Click();
                this.driver.FindElement(this.restoreBy).Click();
            }
        }

        public void RemoveAll()
        {
            this.driver.FindElementWithTimeOutDefault(this.removeAllBy).Click();
        }

        public void BackPage(string url)
        {
            this.driver.Navigate().GoToUrl(url);
        }

        private IWebElement SearchElement(string path)
        {
           var content = this.driver.FindElementWithTimeOutDefault(this.contentBy);
           By file = By.XPath(string.Format("//div[contains(text(), '{0}')]", System.IO.Path.GetFileName(path)));
           return content.FindElement(file);
        }
    }
}
