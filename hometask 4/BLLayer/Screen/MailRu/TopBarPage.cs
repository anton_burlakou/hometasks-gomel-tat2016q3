﻿//-----------------------------------------------------------------------
// <copyright file="TopBarPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using OpenQA.Selenium;
using FrameworkLayer.Browser;

namespace FrameworkLayer.MailRu.POM
{
    public class TopBarPage
    {
        private IWebDriver driver;

        private By greeting = By.Id("PH_user-email");

        public TopBarPage()
        {
            this.driver = DriverInstance.GetInstance();
        }

        public bool IsLogged()
        {
            var element = this.driver.FindElementWithTimeOutDefault(this.greeting);
            if (element != null && element.Displayed)
            {
                return true;
            }

            return false;
        }
    }
}
