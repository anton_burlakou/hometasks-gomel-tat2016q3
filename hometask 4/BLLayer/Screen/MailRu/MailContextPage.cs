﻿//-----------------------------------------------------------------------
// <copyright file="MailContextPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Browser;
using FrameworkLayer.Models;
using OpenQA.Selenium;
using System;
using System.Text.RegularExpressions;

namespace FrameworkLayer.MailRu.POM
{
    public class MailContextPage
    {
        private IWebDriver driver;

        private By mailbox = By.XPath("//div[@id='b-letters']/div[1]/div[not(contains(@style, 'display: none'))]//div[@data-bem='b-datalist__item']");

        private By checkboxMail = By.ClassName("b-checkbox__box");

        private string pattern = @"<Без темы>";

        public MailContextPage()
        {
            this.driver = DriverInstance.GetInstance();
        }

        public bool CheckMail(Mail mail)
        {
            var now = DateTime.Now;
            bool check = true;
            while (DateTime.Now - now < TimeSpan.FromMinutes(2))
            {
                var element = this.driver.FindElementWithTimeOutDefault(this.mailbox);
                if (element.Displayed)
                {
                    IWebElement topicElement;
                    if (mail.Topic == null)
                    {
                        topicElement = element.FindElement(By.ClassName("b-datalist__item__subj"));
                        TimeSpan dateTimeOfMail = TimeSpan.Parse(element.FindElement(By.ClassName("b-datalist__item__date__value")).Text);
                        //// not correct time for later mail
                        if (Regex.IsMatch(topicElement.Text, this.pattern) && dateTimeOfMail.Hours == mail.Timestamp.Hours && dateTimeOfMail.Minutes == mail.Timestamp.Minutes)
                        {
                            check = true;
                        }
                        else
                        {
                            check = false;
                        }
                    }
                    else
                    {
                        var topic = string.Format("//*[contains(text(), {0})]", mail.Topic);
                        By topicBy = By.XPath(topic);
                        topicElement = element.FindElement(topicBy);
                    }

                    if (check)
                    {
                        break;
                    }

                    this.driver.Navigate().GoToUrl(this.driver.Url);
                }
            }

            return check;
        }

        public bool IsSearchMail(Mail mail)
        {
            try
            {
                var element = this.driver.FindElementWithTimeOutDefault(this.mailbox);
                if (element.Displayed)
                {
                    var topic = string.Format("//*[contains(text(), {0})]", mail.Topic);
                    By topicBy = By.XPath(topic);
                    var topicElement = element.FindElement(topicBy);
                    if (topicElement.Displayed)
                    {
                        var checkbox = element.FindElement(this.checkboxMail);
                        checkbox.Click();
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }

            return false;
        }
    }
}
