﻿//-----------------------------------------------------------------------
// <copyright file="LoginPage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Browser;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using FrameworkLayer.Models;
using FrameworkLayer.POM;

namespace FrameworkLayer.MailRu.POM
{
    public class LoginPage : ILoginPage
    {
        private readonly string baseUrl = System.Configuration.ConfigurationManager.AppSettings["StartPageMail"];

        private IWebDriver driver;

        #pragma warning disable 0649
        [FindsBy(How = How.Id, Using = "mailbox__login")]
        private IWebElement inputLogin;

        [FindsBy(How = How.Id, Using = "mailbox__password")]
        private IWebElement inputPassword;

        [FindsBy(How = How.Id, Using = "mailbox__auth__button")]
        private IWebElement buttonSubmit;
        #pragma warning restore 0649

        public LoginPage()
        {
            this.driver = DriverInstance.GetInstance();
            PageFactory.InitElements(this.driver, this);
        }

        public void OpenPage()
        {
            this.driver.Navigate().GoToUrl(this.baseUrl);
        }

        public void Login(Account account)
        {
            this.inputLogin.SendKeys(account.Login);
            this.inputPassword.SendKeys(account.Password);
            this.buttonSubmit.Click();
        }
    }
}
