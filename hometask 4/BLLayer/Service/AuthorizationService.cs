﻿//-----------------------------------------------------------------------
// <copyright file="AuthorizationService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <author>Anton Burlakou</author>
//-----------------------------------------------------------------------

using FrameworkLayer.Models;
using FrameworkLayer.MailRu.POM;
using FrameworkLayer.POM.YandexDisk;
using FrameworkLayer.Utils;
using FrameworkLayer.POM;

namespace FrameworkLayer.Service
{
    public class AuthorizationService : IAuthorizationService
    {
        private Account validAccount;

        private Account incorrectAccount;

       ////private LoginPage loginPage = new LoginPage();

        public AuthorizationService(Models.Service service)
        {
            var login = (service == Models.Service.Mail) ?
                System.Configuration.ConfigurationManager.AppSettings["LoginMail"] :
                System.Configuration.ConfigurationManager.AppSettings["LoginYandex"];
            var password = System.Configuration.ConfigurationManager.AppSettings["Password"];
            this.validAccount = new Account(login, password);
            var random = new System.Random();
            this.incorrectAccount = new Account(RandomGenerator.GetRandomString(random.Next(0, 10)), RandomGenerator.GetRandomString(random.Next(0, 10)));
        }

        public void CorrectLogin(Models.Service service)
        {
            ILoginPage loginPage;
            if (service == Models.Service.Mail)
            {
                loginPage = new MailRu.POM.LoginPage();
            }
            else
            {
                loginPage = new POM.YandexDisk.LoginPage();
            }

            loginPage.OpenPage();
            loginPage.Login(this.validAccount);
        }

        public void IncorrectLogin(Models.Service service)
        {
            ILoginPage loginPage;
            if (service == Models.Service.Mail)
            {
                loginPage = new MailRu.POM.LoginPage();
            }
            else
            {
                loginPage = new POM.YandexDisk.LoginPage();
            }

            loginPage.OpenPage();
            loginPage.Login(this.incorrectAccount);
        }

        public bool IsLoggedIn()
        {
            var topbar = new MailRu.POM.TopBarPage();
            return topbar.IsLogged();
        }
    }
}
