﻿// <copyright file="FileService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrameworkLayer.POM.YandexDisk;
using FrameworkLayer.Utils;
using System.Threading;

namespace FrameworkLayer.Service
{
    public class FileService : IFileService
    {
        private List<string> filepathList;
        private List<string> hashFileList;
        private string url;

        public FileService()
        {
            ////this.filepathList = RandomGenerator.GetRandomFiles(new Random().Next(2, 5));
            ////this.hashFileList = new List<string>(this.filepathList).Select(h => h = RandomGenerator.GetMD5(h)).ToList();
        }

        public void Download()
        {
            var content = new ContentPage();
            content.Download(this.filepathList.FirstOrDefault());
        }

        public bool IsFileUpload()
        {
            var popup = new UploadPopupPage();
            bool check = false;
            var start = DateTime.Now;
            while (true)
            {
                check = popup.IsUpload();
                if (check || DateTime.Now - start > TimeSpan.FromSeconds(60))
                {
                    break;
                }
            }

            popup.Close();
            return check;
        }

        public bool IsFileUpload(List<bool> files)
        {
            return files.All(f => f == true);
        }

        public void UploadFile()
        {
            this.TransformFileList(1);
            var topbar = new TopBarPage();
            topbar.Upload(this.filepathList.FirstOrDefault());
        }

        public List<bool> UploadFile(bool multiupload = false)
        {
            var topbar = new TopBarPage();
            var popup = new UploadPopupPage();
            if (multiupload)
            {
                this.TransformFileList(new Random().Next(2, 5));
                var checklistFilesUpload = Enumerable.Repeat(false, this.filepathList.Count).ToList();

                for (var i = 0; i < this.filepathList.Count; i++)
                {
                    topbar.Upload(this.filepathList[i]);
                    checklistFilesUpload[i] = this.IsFileUpload();
                }

                return checklistFilesUpload;
            }
            else
            {
                this.TransformFileList(1);
                topbar.Upload(this.filepathList.FirstOrDefault());
                return Enumerable.Repeat(this.IsFileUpload(), 1).ToList();
            }
        }

        public bool IsCorrectFile()
        {
            var filename = System.IO.Path.GetFileName(this.filepathList.FirstOrDefault());
            var path = System.IO.Path.Combine(System.Configuration.ConfigurationManager.AppSettings["FolderForDownload"], filename);
            var check = false;
            var start = DateTime.Now;
            while (true)
            {
                if (System.IO.File.Exists(path))
                {
                    check = true;
                }

                if (check || DateTime.Now - start > TimeSpan.FromSeconds(60))
                {
                    break;
                }
            }

            //// for file access need several try
            //// http://stackoverflow.com/questions/26741191/ioexception-the-process-cannot-access-the-file-file-path-because-it-is-being
            var numberOfRetries = 10;
            for (int i = 1; i <= numberOfRetries; i++)
            {
                try
                {
                    if (check && this.hashFileList.FirstOrDefault() == RandomGenerator.GetMD5(path))
                    {
                        return true;
                    }
                }
                catch (System.IO.IOException)
                {
                    if (i == numberOfRetries)
                    {
                        return false;
                    }

                    Thread.Sleep(numberOfRetries * 1000);
                }
            }

            return false;
        }

        public void MovedToTrash()
        {
            var content = new ContentPage();
            content.MovedToTrash(this.filepathList.FirstOrDefault());
            this.url = content.GetCurrentUrl();
        }

        public void MovedToTrash(bool multiupload)
        {
            var content = new ContentPage();
            content.MovedToTrashFilesWithCtrlClick(this.filepathList);
            this.url = content.GetCurrentUrl();
        }

        public bool IsMovedToTrash(bool multiupload)
        {
            if (multiupload)
            {
                var content = new ContentPage();
                content.OpenTrash();
                var trash = new TrashPage();
                return trash.IsExist(this.filepathList);
            }
            else
            {
                return this.IsMovedToTrash();
            }
        }

        public bool IsMovedToTrash()
        {
            var content = new ContentPage();
            content.OpenTrash();
            var trash = new TrashPage();
            return trash.IsExist(this.filepathList.FirstOrDefault());
        }

        public void RemoveFile()
        {
            var trash = new TrashPage();
            trash.RemoveAll();
            var trashPopup = new TrashPopupPage();
            trashPopup.Confirm();
        }

        public bool IsFileRemove()
        {
            var trash = new TrashPage();
            return trash.IsExist(this.filepathList.FirstOrDefault());
        }

        public void Restore()
        {
            var trash = new TrashPage();
            trash.Restore(this.filepathList.FirstOrDefault());
            trash.BackPage(this.url);
        }

        public bool IsRestore()
        {
            var content = new ContentPage();
            return content.IsRestore(this.filepathList.FirstOrDefault());
        }

        private void TransformFileList(int count)
        {
            this.filepathList = RandomGenerator.GetRandomFiles(count);
            this.hashFileList = new List<string>(this.filepathList).Select(h => h = RandomGenerator.GetMD5(h)).ToList();
        }
    }
}
